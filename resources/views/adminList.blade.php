<?php
	use App\User;
?>

@extends ('layouts.app')

@section ('content')
	<div class="row">
		<script>
            function ConfirmDelete()
            {
                var x = confirm("Are you sure you want to delete?");
                if (x)
                    return true;
                else
                    return false;
            }
        </script>
		@forelse($admins as $admin)
			@if($admin->userType == "admin")
				<div class="col-md-4 col-md-offset-4">
					<div class="panel panel-default">
						<div class="panel-heading">
							{{$admin->faculty_id}}
						</div>
						<div class="panel-body">
							{{$admin->fullName}}
						</div>
						<div class="panel-footer clearfix" style="background-color: white;">
							@if($admin->id == Auth::id())
								@if(User::where('userType', 'admin')->count() > 1)
									{{Form::open(['route' => ['deleteAdmin', $admin->id], 'method' => 'DELETE', 'onsubmit' => 'return ConfirmDelete()'])}}
									{{csrf_field()}}
									<button type="submit" class="btn btn-danger btn-sm">Delete</button>
									{{csrf_field()}}
									{{Form::close()}}
								@endif
							@endif
						</div>
					</div>
				</div>
			@endif
		@empty
			No Admin. Really? How'd you get in?!
		@endforelse
	</div>
@endsection