@extends('layouts.app2')
@section('content')

    <div class="row">
        <div class="col-xs-1 col-sm-1"  align="end">
            <eenter><img src="/images/scuaa.jpg" height="100dpx" width="100dpx"></eenter>
        </div>
        <div class="cols-xs-1 col-sm-1">
            <div><center><h3><b>SCUAA REGIONAL/NATIONAL GAMES</b></h3></center></div>
            <div ><center><p>Regional: Pangasinan State University-December 12-16, 2016</p>
                    <p>National: Bicol University – February 19-24, 2017</p>
                </center>
            </div>
            <div><p><b><i>Theme</i>:</b></p></div>
        </div>
        <div class="col-xs-1 col-sm-1">
            <div style="border: solid;"><center>SCUAA FORM 3</center></div>
            <br>
            <div style="border: solid;height:30%;width:100%"><img class="img-responsive img-thumbnail" style="height:100%;width:100%"src=""></div>
        </div>
    </div>
    <div class="row">
        <div class="row">
            <div class="col-xs-4" >&nbsp;</div>
            <div class="col-xs-4" ><center><u><h3><b>ELIGIBITY FORM</b></h3></u></center></div>
        </div>
    </div>
    <div class="row">
        <div class="row">
            <div class="col-xs-4" >&nbsp;</div>
            <div class="col-xs-4" ><center><b>EVENT: <u></u></b><u></u></center></div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12" style="border: groove"><h4><center><b>PARTICIPANTS PERSONAL INFORMATION</b></center></h4></div>
    </div>
    <div class="row">
        <div class="col-xs-8" style="border: groove">NAME OF ATHLETE:</div>
        <div class="col-xs-4" style="border: groove">AGE:</div>
    </div>
    <div class="row">
        <div class="col-xs-4" style="border: groove">DATE OF BIRTH:{</div>
        <div class="col-xs-4" style="border: groove">WEIGH:</div>
        <div class="col-xs-4" style="border: groove">HEIGHT:</div>
    </div>
    <div class="row">
        <div class="col-xs-2" style="border: groove">BLOOD TYPE:</div>
        <div class="col-xs-6" style="border: groove">ALLERGIES:</div>
        <div class="col-xs-4" style="border: groove">MEDICATION IF ANY:</div>
    </div>
    <div class="row">
        <div class="col-xs-8" style="border: groove">Address:</div>
        <div class="col-xs-4" style="border: groove">Contact Number:</div>

    </div>
    <div class="row">
        <div class="col-xs-12" style="border: groove"><h4><center><b>EDUCATIONAL BACKGROUD</b></center></h4></div>
    </div>
    <div class="row">
        <div class="col-xs-4" style="border:groove">&nbsp;</div>
        <div class="col-xs-4" style="border: groove"><center><b>NAME OF SCHOOL</b></center></div>
        <div class="col-xs-4" style="border: groove"><center><b>YEAR GRADUATED</b></center></div>
    </div>
    <div class="row">
        <div class="col-xs-4" style="border: groove">ELEMENTARY</div>
        <div class="col-xs-4" style="border: groove">&nbsp;</div>
        <div class="col-xs-4" style="border: groove">&nbsp;}</div>
    </div>
    <div class="row">
        <div class="col-xs-4" style="border: groove">SECONDAY</div>
        <div class="col-xs-4" style="border: groove">&nbsp;</div>
        <div class="col-xs-4" style="border: groove">&nbsp;</div>
    </div>
    <div class="row">
        <div class="col-xs-4" style="border: groove">TERTIARY/VOCATIONAL</div>
        <div class="col-xs-4" style="border: groove">&nbsp;</div>
        <div class="col-xs-4" style="border: groove">&nbsp;</div>
    </div>
    <div class="row">
        <div class="col-xs-4" style="border: groove">IF TRANSFEREE</div>
        <div class="col-xs-4" style="border: groove">&nbsp;</div>
        <div class="col-xs-4" style="border: groove">&nbsp;</div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <span>====================</span>
            <span>====================</span>
            <span>====================</span>
            <span>====================</span>
            <span>====================</span>
            <span>====================</span>
            <span>===============</span>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-xs-12"><center><u><h2><b>MEDICAL CERTIFICATE</b></h2></u></center></div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <h4 style="text-indent:40px">
                This is to certify that _______________________________ is Physically Fit to participate in the forthcoming
                <b>SCUAA REGIONAL/NATIONAL GAMES</b> scheduled on ____________________________ in _______________________________.
            </h4>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-xs-8">&nbsp;</div>
        <div class="col-xs-4">
            <div><p>___________________________________________</p></div>
            <div><p><center>Printed Name and Signature of Physician</center></p></div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <span>====================</span>
            <span>====================</span>
            <span>====================</span>
            <span>====================</span>
            <span>====================</span>
            <span>====================</span>
            <span>===============</span>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-xs-12"><center><u><h2><b>ATHLETES WAIVER AND RELEASE AGREEMENT</b></h2></u></center></div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <h4 >
                In consideration of the acceptance of my entry, myself, my heirs, executors, administrator
                s and assigns, do hereby release and discharge the organizers of the <b>SCUAA REGIONAL/NATIONAL GAMES 2016</b>,
                assisting groups of private or government agencies, the Commission of Higher Education, and other
                concerned institutions, respective schools and officials, and other parties, individual or group,
                from all claims and damages, demands or actions whatsoever in any manner arising from or growing out of my
                participation in, or while traveling to and from the above-mentioned cultural competition. I further attest and
                verify that I have obtained the necessary
                clearance from my medical doctor and guaranteed <b><i>Physically Fit</i></b>  to participate in the said sports competition.
            </h4>
        </div>
    </div>
    <br>
    <br>
    <div class="row">
        <div class="col-xs-12"><center><u><h3><b>PARENT/GUARDIAN PERMIT</b></h3></u></center></div>
    </div>
    <br>
    <div class="row">
        <div class="col-xs-12">
            <h4 >
                This is to certify that I have full knowledge of and permission for my son/daughter/foster child to join
                and participate in the <b> SCUAA REGIONAL/NATIONAL GAMES</b>.
                I concur and agree on the rules, policies and regulations being implemented by the concerned organizers.
            </h4>
        </div>
    </div>
    <br>
    <br>
    <div class="row">
        <div class="col-xs-4">
            <div style="border-bottom: groove"></div>
            <div><center>Athletes’ Signature
                    (Signature over printed name)
                </center></div>
            <div><p>Contact No:__________________________</p></div>
        </div>


        <div class="col-xs-4">&nbsp;</div>
        <div class="col-xs-4">
            <div style="border-bottom: groove"></div>
            <div><center>
                    Parent/Guardian Signature
                    (Signature over printed name)
                </center></div>
            <div><p>Contact No:__________________________</p></div>
        </div>
    </div>
    <div class="page-break"></div>

@endsection