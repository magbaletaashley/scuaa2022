﻿@extends('layouts.app2')
@section('content')
    <br>

    <div class="row">
       <div class="col-xs-3"  align="end">
           <eenter><img class="img-responsive" src="/images/scuaa.jpg" height="200dpx" width="200dpx"></eenter>
       </div>
       <div class=" col-xs-6">
           <div><center><h3><b>SCUAA REGIONAL/NATIONAL GAMES</b></h3></center></div>
           <div ><center><h5>Regional: Pangasinan State University-December 12-16, 2016</h5>
               <h5>National: Batangas State University – February 19-24, 2017</h5>
               </center>
           </div>
           <div><hp><b><i>Theme</i>:</b></p></div>
       </div>
       <div class=" col-xs-3">
           <div style="border: solid; width: 2in"><center>SCUAA FORM 3</center></div>
           <br>
           <div style="border: solid;height:2in;width:2in"><img class="img-responsive img-thumbnail" style="height:100%;width:100%"src="{{$info->pic}}"></div>
       </div>
    </div>
    <div class="row">
        <div class="row">
            <div class="col-xs-4" >&nbsp;</div>
            <div class="col-xs-4" ><center><u><p style="font-size:25px "><b>ELIGIBILITY FORM</b></p></u></center></div>
        </div>
    </div>
    <div class="row">
        <div class="row">
            <div class="col-xs-4" >&nbsp;</div>
            <div class="col-xs-4" ><p><center><b>EVENT: <u>{{$info->events->first()->name->first()->desc}} </u></b><u></u></center></p></div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12" style="border: groove"><p><center><b>PARTICIPANTS PERSONAL INFORMATION</b></center></p></div>
    </div>
    <div class="row">
        <div class="col-xs-8" style="border: groove"><p>NAME OF ATHLETE: {{$info->lname}},{{$info->fname}} {{$info->mname}}</p></div>
        <div class="col-xs-4" style="border: groove"><p>AGE:{{$info->age}}</p></div>
    </div>
    <div class="row">
        <div class="col-xs-4" style="border: groove"><p>DATE OF BIRTH:{{ \Carbon\Carbon::parse($info->dob)->format('M-d-Y')}}</p></div>
        <div class="col-xs-4" style="border: groove"><p>WEIGHT:{{$info->weight}}</p></div>
        <div class="col-xs-4" style="border: groove"><p>HEIGHT:{{$info->height}}</p></div>
    </div>
    <div class="row">
        <div class="col-xs-3" style="border: groove"><p>BLOOD TYPE: {{$info->blood_type}}</p></div>
        <div class="col-xs-5" style="border: groove"><p>ALLERGIES:{{$info->allergies}}</p></div>
        <div class="col-xs-4" style="border: groove"><p>MEDICATION IF ANY:{{$info->medication}}</p></div>
    </div>
    <div class="row">
        <div class="col-xs-8" style="border: groove"><p>Address:{{$info->address}}</p></div>
        <div class="col-xs-4" style="border: groove"><p>Contact Number:{{$info->contact}}</p></div>

    </div>
    <div class="row">
        <div class="col-xs-12" style="border: groove"><h5><center><b>EDUCATIONAL BACKGROUD</b></center></h5></div>
    </div>
    <div class="row">
        <div class="col-xs-4" style="border:groove"><p>&nbsp;</p></div>
        <div class="col-xs-4" style="border: groove"><center><p><b>NAME OF SCHOOL</b></p></center></div>
        <div class="col-xs-4" style="border: groove"><center><p><b>YEAR GRADUATED</b></p></center></div>
    </div>
    <div class="row">
        <div class="col-xs-4" style="border: groove"><p>ELEMENTARY</p></div>
        <div class="col-xs-4" style="border: groove"><p>{{$info->elem->first()->name}}</p></div>
        <div class="col-xs-4" style="border: groove"><p>{{$info->elem->first()->year_grad}}</p></div>
    </div>
    <div class="row">
        <div class="col-xs-4" style="border: groove"><p>SECONDARY</p></div>
        <div class="col-xs-4" style="border: groove"><p>{{$info->high->first()->name}}</p></div>
        <div class="col-xs-4" style="border: groove"><p>{{$info->high->first()->year_grad}}</p></div>
    </div>
    <div class="row">
        <div class="col-xs-4" style="border: groove"><p>TERTIARY/VOCATIONAL</p></div>
        <div class="col-xs-4" style="border: groove"><p>{{$info->col->count()?$info->col->first()->name:'&nbsp;'}}</p></div>
        <div class="col-xs-4" style="border: groove"><p>{{$info->col->count()?$info->col->first()->year_grad:'&nbsp;'}}</p></div>
    </div>
    <div class="row">
        <div class="col-xs-4" style="border: groove"><p>IF TRANSFEREE</p></div>
        <div class="col-xs-4" style="border: groove"><p>{{$info->trans->count()?$info->trans->first()->name:'&nbsp;'}}</p></div>
        <div class="col-xs-4" style="border: groove"><p>{{$info->trans->count()?$info->trans->first()->year_grad:'&nbsp;'}}</p></div>
    </div>
 <div class="row">
        <div class="col-xs-12" align="center">
            <span>-------------------</span>
            <span>-------------------</span>
            <span>-------------------</span>
            <span>-------------------</span>
            <span>-------------------</span>
            <span>-------------------</span>
            <span>-------------------</span>
            <span>-------------------</span>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12"><center><u><h5 style="font-size:20px "><b>MEDICAL CERTIFICATE</b></h5></u></center></div>
    </div>
    <div class="row">
        <div class="col-xs-12">
         <h6 style="text-indent:40px;font-size:18px ">
             This is to certify that _______________________________ is Physically Fit to participate in the forthcoming
             <b>SCUAA REGIONAL/NATIONAL GAMES</b> scheduled on____________________________ in _______________________________.
         </h6>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-xs-8">&nbsp;</div>
        <div class="col-xs-4">
            <div><p>___________________________________________</p></div>
            <div><center><p>Physician</p></center></div>
        </div>
    </div>
 <div class="row">
        <div class="col-xs-12" align="center">
          <span>-------------------</span>
          <span>-------------------</span>
          <span>-------------------</span>
          <span>-------------------</span>
          <span>-------------------</span>
          <span>-------------------</span>
          <span>-------------------</span>
          <span>-------------------</span>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12"><center><u><h5 style="font-size:20px "><b>ATHLETES WAIVER AND RELEASE AGREEMENT</b></h5></u></center></div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <h6 style="text-indent: 40px;font-size:16px ">
                In consideration of the acceptance of my entry, myself, my heirs, executors, administrator
                s and assigns, do hereby release and discharge the organizers of the <b>SCUAA REGIONAL/NATIONAL GAMES 2017</b>,
                assisting groups of private or government agencies, the Commission of Higher Education, and other
                concerned institutions, respective schools and officials, and other parties, individual or group,
                from all claims and damages, demands or actions whatsoever in any manner arising from or growing out of my
                participation in, or while traveling to and from the above-mentioned cultural competition. I further attest and
                verify that I have obtained the necessary
                clearance from my medical doctor and guaranteed <b><i>Physically Fit</i></b>  to participate in the said sports competition.
            </h6>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12"><center><h5 style="font-size:20px "><b>PARENT/GUARDIAN PERMIT</b></h5></center></div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <h6 style="text-indent: 40px; font-size:16px">
                This is to certify that I have full knowledge of and permission for my son/daughter/foster child to join
                and participate in the <b> SCUAA REGIONAL/NATIONAL GAMES</b>.
                I concur and agree on the rules, policies and regulations being implemented by the concerned organizers.
            </h6>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-xs-4">
            <div style="border-bottom: groove"></div>
            <div><center>Athletes’ Signature over printed name
                </center></div>
            <div><p>Contact No:_____________</p></div>
        </div>


        <div class="col-xs-4">&nbsp;</div>
        <div class="col-xs-4">
            <div style="border-bottom: groove"></div>
            <div><center>
                    Parent Signature over printed name
                </center></div>
            <div><p>Contact No:________________</p></div>
        </div>
    </div>
<script>
    window.print();
</script>

@endsection
