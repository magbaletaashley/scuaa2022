﻿@extends('layouts.app2')
@section('content')
    <br>
	
	<style>
		.tab {
			position: absolute;
			left: 25em;
		}
		.tabs {
			position: absolute;
			left: 5em;
		}
		.smalltab {
			position: absolute;
			left: 28em;
		}
		.smalltabs {
			position: absolute;
			left: 8em;
		}
		.small {
			position: absolute;
			left: 2em;
		}
	</style>
	
    <div class="row">
       <div class="col-xs-3"  align="end">
           <center><img class="img-responsive" src="/images/scuaa.jpg" height="150dpx" width="150dpx"></center>
       </div>
       <div class=" col-xs-6">
			<div><center><h2><b>SCUAA-I OLYMPICS 2022</b></h2></center></div>
			<div><center><h4><b>Host: PANGASINAN STATE UNIVERSITY (PSU)</b></h4></center></div>
			<div><center><h4><b>Main Campus, Alvear E, Poblacion, Lingayen, Pangasinan</b></h4></center></div>
			<div><center><h4><b>November 21-25, 2022</b></h4></center></div>
       </div>
       <div class=" col-xs-3">
           <div style="border: solid; width: 2in"><center>SCUAA FORM 3</center></div>
           <br>
		   <div style="border: solid;height:2in;width:1.8in"><img class="img-responsive img-thumbnail" style="height:100%;width:100%"src="{{$info->pic}}"></div>
       </div>
    </div>
    <div class="row">
        <div class="row">
            <div class="col-xs-4" >&nbsp;</div>
            <div class="col-xs-4" ><center><p style="font-size:25px"><b><u>ELIGIBILITY FORM</b></u></p></center></div>
        </div>
    </div>
    <div class="row">
        <div class="row">
            <div class="col-xs-4" ><p style="font-size:23px"><left><b><h4>&nbsp;&nbsp;EVENT: {{$e}} </b></left></p></h4></div>
            <div class="col-xs-4" >&nbsp;</div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12" style="border: groove"><p style="font-size:24px"><center><b><h3><u>PARTICIPANT'S PERSONAL INFORMATION</u></h3></b></center></p></div>
    </div>
    <div class="row">
        <div class="col-xs-8" style="border: groove"><p style="font-size:20px">NAME OF ATHLETE: <b>{{strtoupper($info->lname.",".$info->fname." ".substr($info->mname,0,1).".")}}</b></p></div>
        <div class="col-xs-4" style="border: groove"><p style="font-size:20px">AGE:{{$info->age}} years old</p></div>
    </div>
    <div class="row">
        <div class="col-xs-4" style="border: groove"><p style="font-size:20px">DATE OF BIRTH:{{ \Carbon\Carbon::parse($info->dob)->format('M. d, Y')}}</p></div>
        <div class="col-xs-4" style="border: groove"><p style="font-size:20px">WEIGHT:{{$info->weight}}</p></div>
        <div class="col-xs-4" style="border: groove"><p style="font-size:20px">HEIGHT:{{$info->height}}</p></div>
    </div>
    <div class="row">
        <div class="col-xs-3" style="border: groove"><p style="font-size:20px">BLOOD TYPE: {{strtoupper($info->blood_type)}}</p></div>
        <div class="col-xs-5" style="border: groove"><p style="font-size:20px">ALLERGIES:{{$info->allergies}}</p></div>
        <div class="col-xs-4" style="border: groove"><p style="font-size:20px">MEDICATION IF ANY:{{$info->medication}}</p></div>
    </div>
    <div class="row">
        <div class="col-xs-8" style="border: groove"><p style="font-size:20px">Address:{{$info->address}}</p></div>
        <div class="col-xs-4" style="border: groove"><p style="font-size:20px">Contact Number:{{$info->contact}}</p></div>

    </div>
    
	<div class="row">
        <div class="col-xs-12" align="center">
          <span>================================================================================================================================================</span>
          
        </div>
    </div>
	</br>
	<div class="row">
        <div class="col-xs-6">
			<div class="col-xs-12" style="border: groove" ><center><p style="font-size:20px"><b><u>MEDICAL CERTIFICATE</u><b></p></center></div>
			<div class="col-xs-12" style="border: groove" >
				<h6 style="text-indent:0px;font-size:18px ">
					This is to certify that: <b>{{strtoupper($info->lname.",".$info->fname." ".substr($info->mname,0,1).".")}} </b>
				</h6>
				</br>
			</div>
			<div class="col-xs-12" style="border: groove" >
				<h6 style="text-indent:0px;font-size:18px ">
					is <b><i>Physically Fit</i></b> to participate in the following competitions;
				</h6>
			</div>
			<div class="col-xs-12" style="border: groove" >
				<h6 style="text-indent:0px;font-size:16px ">
				     [&emsp;] <b>&emsp;&emsp;&emsp;&emsp;: SCUAA-I OLYMPICS 2022 </b>&emsp;on 
				</h6>
			</div>
			<div class="col-xs-12" style="border: groove" >
				<h6 style="text-indent:0px;font-size:14px ">
				     (date)	 <b>&emsp;&emsp;&emsp;&emsp;November 21-25, 2022 </b>&emsp;at 
				</h6>
			</div>
			<div class="col-xs-12" style="border: groove" >
				<h6 style="text-indent:0px;font-size:14px ">
				     (venue) <b>&emsp;&emsp;&emsp;PANGASINAN STATE UNIVERSITY, Lingayen, Pangasinan </b>										
				</h6>
			</div>
			<div class="col-xs-12" style="border: groove" >
				<h6 style="text-indent:0px;font-size:16px ">
				     &emsp; <b>&emsp;&emsp;&emsp;&emsp; </b>&emsp;
				</h6>
			</div>
			<div class="col-xs-12" style="border: groove" >
				<h6 style="text-indent:0px;font-size:14px ">
				     	 <b>&emsp;&emsp;&emsp;&emsp;  </b>&emsp;
				</h6>
			</div>
			<div class="col-xs-12" style="border: groove" >
				<h6 style="text-indent:0px;font-size:14px ">
				      <b>&emsp;&emsp;&emsp;&emsp; </b>											
				</h6>
			</div>
			<div class="col-xs-12" >
				<div class="row" >
					<div class="col-xs-6" style="border: groove" >
						<h6 style="text-indent:0px;font-size:12px ">
							Blood Pressure:	
						</h6>
						</br></br></br>
					</div>
					<div class="col-xs-6">
						<div class="row" style="border: groove" >
							</br></br>
						</div>
						<div class="row" style="border: groove" >
							<h6 style="text-indent:0px;font-size:12qpx ">
								<center><b> Name and Signature of </br> Physician </b></center>										
							</h6>
						</div>
					</div>
					
				</div>
			</div>
			<div class="col-xs-12" >
				<div class="row" >
					<div class="col-xs-6" style="border: groove" >
						<h6 style="text-indent:0px;font-size:12px ">
							Date of Examination:										
						</h6>
						</br></br>
					</div>
					<div class="col-xs-3" style="border: groove" >
						</br></br>
						<h6 style="text-indent:0px;font-size:12px ">
							<center>License Number</center>										
						</h6>
					</div>
					<div class="col-xs-3" style="border: groove" >
						</br></br>
						<h6 style="text-indent:0px;font-size:12px ">
							<center>Validity Date</center>										
						</h6>
					</div>
				</div>
			</div>
				
		
		</div>
		<div class="col-xs-6" >
			<div class="row" >
			    
				<div class="col-xs-12" style="border: groove" ></br><center><p style="font-size:20px"><b><u>ATHLETE'S WAIVER AND RELEASE AGREEMENT</u><b></p></center>
			
				</br>
			
				<h6 style="text-indent: 40px;font-size:20px;  text-align: justify">
					In consideration of the acceptance of my entry, myself, my heirs, executors, administrators
					and assigns, do hereby release and discharge the organizers of the <b>SCUAA-I OLYMPICS 2022</b>,
					assisting groups of private or government agencies, the Commission of Higher Education, and other
					concerned institutions, respective schools and officials, and other parties, individual or group,
					from all claims and damages, demands or actions whatsoever in any manner arising from or growing out of my
					participation in, or while traveling to and from the above-mentioned sports competition. I further attest and
					verify that I have obtained the necessary
					clearance from my medical doctor and guaranteed <b><i>Physically Fit</i></b>  to participate in the said sports competition.
					
				</h6>
				</br></br>
				</div>
			</div>
			<div class="row" >
				<div class="col-xs-6" style="border: groove"><center><p style="font-size:20px"></p></br></br>{{strtoupper($info->lname.",".$info->fname." ".substr($info->mname,0,1).".")}}</center></div>
				<div class="col-xs-6" style="border: groove"><center><p style="font-size:20px"></p></br></br>{{$info->contact}}</center></div>
			</div>
			<div class="row" >
				<div class="col-xs-6" style="border: groove"><center><p style="font-size:20px"></p><b>Name and Signature of </br>Athlete</b></br></br></center></div>
				<div class="col-xs-6" style="border: groove"><center><p style="font-size:20px"></p><b>Contact Number </b></br></br></br></center></div>
			</div>
		</div> 
    </div>
	</b>
	<div class="row">
        <div class="col-xs-12" align="center">
          <span>================================================================================================================================================</span>
          
        </div>
    </div>
	</br>
    <div class="row">
        <div class="col-xs-12" style="border: groove">
			</br>
			<center><h5 style="font-size:20px "><b>PARENT/GUARDIAN PERMIT</b></h5></center>
			<h6 style="text-indent: 40px; font-size:18px;text-align: justify">
				This is to certify that I have full knowledge of and permission for my son/daughter/foster child to join and participate in the following competitions:
			</h6>
		</div>
    </div>
	<div class="row">
        <div class="col-xs-12" style="border: groove">
			<h6 style="text-indent: 0px; font-size:18px">
				[&emsp;] &emsp; <b>: SCUAA-I OLYMPICS 2022 </b> &emsp; on &emsp; <b>November 21-25, 2022</b> &emsp; at the &emsp; <b>PANGASINAN STATE UNIVERSITY, Lingayen, Pangasinan </b>
			</h6>
		</div>
    </div> 
    <div class="row">
        <div class="col-xs-12" style="border: groove">
			<h6 style="text-indent: 0px; font-size:14px">
				<b>&emsp;&emsp;&emsp;&emsp; </b>	
			</h6>
		</div>
    </div>
	<div class="row">
        <div class="col-xs-12" style="border: groove">
			<h6 style="text-indent: 0px; font-size:18px">
				I concur and agree on the rules, policies and regulations being implemented by the concerned organizers. 
			</h6>
		</div>
    </div>
	<div class="row">
        <div class="col-xs-6" style="border: groove"></br></br></div>
        <div class="col-xs-6" style="border: groove"></br></br></div>
    </div>
	<div class="row">
        <div class="col-xs-6" style="border: groove">
			<h6 style="text-indent: 0px; font-size:18px">
				<center>Name and Signature of Parent/Guardian</center>
			</h6>
			</br>
		</div>
        <div class="col-xs-6" style="border: groove">
			<h6 style="text-indent: 0px; font-size:18px">
				<center>Contact Number</center>
			</h6>
			</br>
		</div>
    </div>
    <br>
	
	<div class="row">
        <div class="col-xs-12" align="center">
          <span>================================================================================================================================================</span>
          
        </div>
    </div>
	<div class="row">
        <div class="col-xs-12">
			<h6 style="text-indent: 40px; font-size:18px">
				Subscribed and Sworn to me this  _____________  day of  __________________  2022 at  _______________________.
			</h6> 
        </div>
    </div>
   <div class="row">
        <div class="col-xs-8"></p></div>
        <div class="col-xs-4">
			<h6 style="text-indent: 40px; font-size:18px">
				<center>___________________________</br>&emsp;Notary</center>
			</h6>
		</div>
    </div>
   </br></br>
<script>
    window.print();
</script>

@endsection
