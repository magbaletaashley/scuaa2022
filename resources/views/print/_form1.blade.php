@extends('layouts.app2')
@section('content')
    <br>
    <br>
    <div class="row">
       <div class="col-xs-3 col-sm-3"  align="end">
           <eenter><img class="img-responsive" src="/images/scuaa.jpg" height="200dpx" width="200dpx"></eenter>
       </div>
       <div class=" col-xs-6">
           <div><center><h3><b>SCUAA REGIONAL/NATIONAL GAMES</b></h3></center></div>
           <div ><center><h4>Regional: Pangasinan State University-December 12-16, 2016</h4>
               <h4>National: Bicol University – February 19-24, 2017</h4>
               </center>
           </div>
           <div><h4><b><i>Theme</i>:</b></h4></div>
       </div>
       <div class=" col-xs-3">
           <div style="border: solid; width: 2in"><center>SCUAA FORM 3</center></div>
           <br>
           <div style="border: solid;height:2in;width:2in"><img class="img-responsive img-thumbnail" style="height:100%;width:100%"src="{{$info->pic}}"></div>
       </div>
    </div>
    <div class="row">
        <div class="row">
            <div class="col-xs-4" >&nbsp;</div>
            <div class="col-xs-4" ><center><u><h3><b>ELIGIBITY FORM</b></h3></u></center></div>
        </div>
    </div>
    <div class="row">
        <div class="row">
            <div class="col-xs-4" >&nbsp;</div>
            <div class="col-xs-4" ><h4><center><b>EVENT: <u>{{$info->events->first()->name->first()->desc}} </u></b><u></u></center></h4></div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12" style="border: groove"><h3><center><b>PARTICIPANTS PERSONAL INFORMATION</b></center></h3></div>
    </div>
    <div class="row">
        <div class="col-xs-8" style="border: groove"><h4>NAME OF ATHLETE: {{$info->lname}},{{$info->fname}} {{$info->mname}}</h4></div>
        <div class="col-xs-4" style="border: groove"><h4>AGE:{{$info->age}}</h4></div>
    </div>
    <div class="row">
        <div class="col-xs-4" style="border: groove"><h4>DATE OF BIRTH:{{ \Carbon\Carbon::parse($info->dob)->format('M-d-Y')}}</h4></div>
        <div class="col-xs-4" style="border: groove"><h4>WEIGHT:{{$info->weight}}</h4></div>
        <div class="col-xs-4" style="border: groove"><h4>HEIGHT:{{$info->height}}</h4></div>
    </div>
    <div class="row">
        <div class="col-xs-2" style="border: groove"><h4>BLOOD TYPE: {{$info->blood_type}}</h4></div>
        <div class="col-xs-6" style="border: groove"><h4>ALLERGIES:{{$info->allergies}}</h4></div>
        <div class="col-xs-4" style="border: groove"><h4>MEDICATION IF ANY:{{$info->medication}}</h4></div>
    </div>
    <div class="row">
        <div class="col-xs-8" style="border: groove"><h4>Address:{{$info->address}}</h4></div>
        <div class="col-xs-4" style="border: groove"><h4>Contact Number:{{$info->contact}}</h4></div>

    </div>
    <div class="row">
        <div class="col-xs-12" style="border: groove"><h3><center><b>EDUCATIONAL BACKGROUD</b></center></h3></div>
    </div>
    <div class="row">
        <div class="col-xs-4" style="border:groove"><h4>&nbsp;</h4></div>
        <div class="col-xs-4" style="border: groove"><center><h4><b>NAME OF SCHOOL</b></h4></center></div>
        <div class="col-xs-4" style="border: groove"><center><h4><b>YEAR GRADUATED</b></h4></center></div>
    </div>
    <div class="row">
        <div class="col-xs-4" style="border: groove"><h4>ELEMENTARY</h4></div>
        <div class="col-xs-4" style="border: groove"><h4>{{$info->elem->first()->name}}</h4></div>
        <div class="col-xs-4" style="border: groove"><h4>{{$info->elem->first()->year_grad}}</h4></div>
    </div>
    <div class="row">
        <div class="col-xs-4" style="border: groove"><h4>SECONDARY</h4></div>
        <div class="col-xs-4" style="border: groove"><h4>{{$info->high->first()->name}}</h4></div>
        <div class="col-xs-4" style="border: groove"><h4>{{$info->high->first()->year_grad}}</h4></div>
    </div>
    <div class="row">
        <div class="col-xs-4" style="border: groove"><h4>TERTIARY/VOCATIONAL</h4></div>
        <div class="col-xs-4" style="border: groove"><h4>{{$info->col->count()?$info->col->first()->name:'&nbsp;'}}</h4></div>
        <div class="col-xs-4" style="border: groove"><h4>{{$info->col->count()?$info->col->first()->year_grad:'&nbsp;'}}</h4></div>
    </div>
    <div class="row">
        <div class="col-xs-4" style="border: groove"><h4>IF TRANSFEREE</h4></div>
        <div class="col-xs-4" style="border: groove"><h4>{{$info->trans->count()?$info->trans->first()->name:'&nbsp;'}}</h4></div>
        <div class="col-xs-4" style="border: groove"><h4>{{$info->trans->count()?$info->trans->first()->year_grad:'&nbsp;'}}</h4></div>
    </div>
 <div class="row">
        <div class="col-xs-12">
            <span>====================</span>
            <span>====================</span>
            <span>====================</span>
            <span>====================</span>
            <span>====================</span>
            <span>================================</span>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12"><center><u><h2><b>MEDICAL CERTIFICATE</b></h2></u></center></div>
    </div>
    <div class="row">
        <div class="col-xs-12">
         <h3 style="text-indent:40px">
             This is to certify that _______________________________ is Physically Fit to participate in the forthcoming
             <b>SCUAA REGIONAL/NATIONAL GAMES</b> scheduled on ____________________________ in _______________________________.
         </h3>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-xs-8">&nbsp;</div>
        <div class="col-xs-4">
            <div><p>___________________________________________</p></div>
            <div><center><p>Printed Name and Signature of Physician</p></center></div>
        </div>
    </div>
 <div class="row">
        <div class="col-xs-12">
            <span>====================</span>
            <span>====================</span>
            <span>====================</span>
            <span>====================</span>
            <span>====================</span>
            <span>================================</span>

        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-xs-12"><center><u><h2><b>ATHLETES WAIVER AND RELEASE AGREEMENT</b></h2></u></center></div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <h3 >
                In consideration of the acceptance of my entry, myself, my heirs, executors, administrator
                s and assigns, do hereby release and discharge the organizers of the <b>SCUAA REGIONAL/NATIONAL GAMES 2016</b>,
                assisting groups of private or government agencies, the Commission of Higher Education, and other
                concerned institutions, respective schools and officials, and other parties, individual or group,
                from all claims and damages, demands or actions whatsoever in any manner arising from or growing out of my
                participation in, or while traveling to and from the above-mentioned cultural competition. I further attest and
                verify that I have obtained the necessary
                clearance from my medical doctor and guaranteed <b><i>Physically Fit</i></b>  to participate in the said sports competition.
            </h3>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-xs-12"><center><u><h2><b>PARENT/GUARDIAN PERMIT</b></h2></u></center></div>
    </div>
    <br>
    <div class="row">
        <div class="col-xs-12">
            <h3 style="text-indent: 10px">
                This is to certify that I have full knowledge of and permission for my son/daughter/foster child to join
                and participate in the <b> SCUAA REGIONAL/NATIONAL GAMES</b>.
                I concur and agree on the rules, policies and regulations being implemented by the concerned organizers.
            </h3>
        </div>
    </div>
    <br>
    <br>
    <div class="row">
        <div class="col-xs-4">
            <div style="border-bottom: groove"></div>
            <div><center>Athletes’ Signature
                    (Signature over printed name)
                </center></div>
            <div><p>Contact No:__________________________</p></div>
        </div>


        <div class="col-xs-4">&nbsp;</div>
        <div class="col-xs-4">
            <div style="border-bottom: groove"></div>
            <div><center>
                    Parent/Guardian Signature
                    (Signature over printed name)
                </center></div>
            <div><p>Contact No:__________________________</p></div>
        </div>
    </div>
<script>
    window.print();
</script>

@endsection