<?php
    use App\PlayerEvents;
    use App\PlayerInfo;
    use App\CoachEvents;
    use App\Coach;
?>

<style>
    @media all {
        .page-break  { display: none; }
        td{
            vert-align: top;
        }
    }

    @media print {
        td{
            vert-align: top;
        }
        .page-break  { display: block; page-break-before: always; }
    }

    .my-table {
        page-break-before: always;
        page-break-after: always;
    }
    .my-table tr {
        page-break-inside: avoid;
    }
    #c{
        border:solid .5px;
    }

    td {
    text-transform: uppercase;
    }

</style>






        {{--*/ $j = 1 /*--}}

<table width="100%" border="0" id="gal" cellspacing="0" cellpadding="0">
    <thead>
        <tr>
            <td colspan="7" align="center">
                <br />
                <div><center><h2>SCUAA REGIONAL/NATIONAL GAMES 2018</h2></center></div>
				<div><center><h4>Host: University of Northern Philippines </br> November 18-22, 2017</h4></center></div>
				<div><center><h3>Master List of Participant (ATHLETE and COACH)</h3></center></div>
            </td>
        <tr>
            <td id="c" width="1%"><center></center></td>
			<td id="c" width="6%"><b><center>Student Number</center></b></td>
            <td id="c" width="20%"><b><center>Name</center></b></td>
			<td id="c" width="5%"><b><center>Age</center></b></td>
			<td id="c" width="10%"><b><center>Participation</center></b></td>
            <td id="c" width="10%"><b><center>Regional Code</center></b></td>
			<td id="c" width="10%"><b><center>University/College Affiliation</center></b></td>
        </tr>
    </thead>
    <tbody>
        <?php
            $a = 0;
        ?>
        @foreach($e as $event)
            <tr>
                <td id="c" colspan="2" align="left">
                    <b>{{ $e[$a]->desc }}</b>
                </td>
				<td id="c" align="left">
                    &nbsp;
                </td>
                <td id="c" align="left">
                    &nbsp;
                </td>
                <td id="c" align="left">
                    &nbsp;
                </td>
				<td id="c" align="left" >
                    &nbsp;
                </td>
				<td id="c" align="left" >
                     &nbsp;
                </td>
            </tr>
            
            <?php
                    $b = 0;
                    $playerEvents = PlayerEvents::where('events_id', ($a + 1))->get();

                    $subset = $playerEvents->map(function ($playerEvents) {
                        return collect($playerEvents->toArray())
                            ->only(['student_number'])
                            ->all();
                    });

                    $players = PlayerInfo::whereIn('student_number', $subset)->orderby('lname', 'ASC')->get();

            ?>
            @foreach($players as $player)
                <tr>
                    <td id="c" align="left">
                        {{ $b + 1 }}
                    </td>
					 <td id="c" align="left" >
                        {{ $player->student_number }}
                    </td>
                    <td id="c" align="left" >
                        {{ $player->lname }}, {{ $player->fname }} {{ substr($player->mname,0,1)}}.
                    </td>
                    <td id="c" align="left" >
                        <center>{{ $player->age }}</center>
                    </td>
					<td id="c" align="left" >
                        <center>ATHLETE</center>
                    </td>
					<td id="c" align="left" >
                        <center>01</center>
                    </td>
					<td id="c" align="left" >
                        <center>MMSU</center>
                    </td>
                </tr>
                <?php
                    $b += 1;
                ?>
            @endforeach
            
            
            <?php
                
                $coachEvents = CoachEvents::where('event', ($a + 1))->get();
                $subset = $coachEvents->map(function ($coachEvents) {
                    return collect($coachEvents->toArray())
                        ->only(['coach_id'])
                        ->all();
                });
                $coaches = Coach::whereIn('id', $subset)->orderby('lname','ASC')->get();
            ?>
            @foreach($coaches as $coach)
                <tr>
                    <td id="c" align="left">
                        {{ $b + 1 }}
                    </td>
					<td id="c" align="left">
                         &nbsp;
                    </td>
                    <td id="c" align="left" >
                        {{ $coach->lname }}, {{ $coach->fname }} {{ substr($coach->mname,0,1) }}
                    </td>
                    <td id="c" align="left" >
                        &nbsp;
                    </td>
                    <td id="c" align="left" >
						<center>COACH</center>
                    </td>
					<td id="c" align="left" >
                        <center>01</center>
                    </td>
					<td id="c" align="left" >
                        <center>MMSU</center>
                    </td>
                </tr>
                <?php
                    $b += 1;
                ?>
            @endforeach
            <tr>
                <td id="c" colspan="5" align="left">
                    &nbsp;
                </td>
				<td id="c" colspan="5" align="left">
                    &nbsp;
                </td>
				
            </tr>
            <tr>
                <td id="c" colspan="5" align="left">
                    &nbsp;
                </td>
				<td id="c" colspan="5" align="left">
                    &nbsp;
                </td>
            </tr>
            <?php
                $a += 1;
            ?>
        @endforeach
    </tbody>
</table>