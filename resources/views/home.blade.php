@extends('layouts.app')

@section('content')
    <div class="container">
        @if(\Illuminate\Support\Facades\Session::has('messages'))
            <div class="row">
                <div class="col-md-2">&nbsp;</div>
                <div class="col-sm-8 alert alert-danger">
                    <center><span >{{\Illuminate\Support\Facades\Session::get('messages')}}</span></center>
                </div>
            </div>
        @endif
        <div class="row">
            {{Form::open(array('url'=>route('eve'),'method'=>'POST'))}}
            {{csrf_field()}}
            <div class="form-group"></div>
            <div class="col-md-4" align="left">
                {{Form::select('events_id', $events->pluck('desc','id'),null,array('class'=>'form-control'))}}
            </div>
            <div class="col-md-2">
                {{Form::submit('Go',array('class'=>'btn btn-success'))}}
            </div>
            {{csrf_field()}}
            {{Form::close()}}
            <div class="col-md-2">&nbsp; </div>
            {{Form::open(array('url'=>route('ser'),'method'=>'POST'))}}
            {{csrf_field()}}
            <div class="col-md-2">

                {{Form::text('student_number',null,array('class'=>'form-control','placeholder'=>'Search'))}}

            </div>
            <div class="col-md-2">
                {{Form::submit('Search',array('class'=>'btn btn-success'))}}
                &nbsp;
                <a href="{{ route('form.create') }}" class='btn btn-primary'>Add</a>
            </div>
            {{csrf_field()}}
            {{Form::close()}}
        </div>

        <div class="row">&nbsp; </div>
		
        <div class="row">
            <div class="col-md-12">
                <table class="table table-responsive table-condensed">
                    <thead>
                    <td>Student Number</td>
                    <td>Name</td>
                    <td>&nbsp;</td>
                    </thead>
                    <script>
                        function ConfirmDelete()
                        {
                          var x = confirm("Are you sure you want to delete?");
                          if (x)
                            return true;
                          else
                            return false;
                        }
                    </script>
                    @foreach($info as $i)
                        <tr>
                            
							<td>{{$i->student_number}}</td>
                            <td class="text-uppercase">{{$i->lname}}, {{$i->fname}} {{substr($i->mname,0,1)}}.</td>
							
                            <td width="3">
                            <?php
                                $parameter = $i->student_number;
                                $parameter= Crypt::encrypt($parameter);
                            ?>
                                {{Form::open(array('url'=>route('form.edit', $parameter) ,'method'=>'GET'))}}
                                {{csrf_field()}}
                                <button type="submit" class="glyphicon glyphicon-edit"></button>
                                {{csrf_field()}}
                                {{Form::close()}}
                            </td>
                            <td width="3">
                                {{Form::open(array('url'=>route('form.destroy',$i->student_number) ,'method'=>'DELETE', 'onsubmit' => 'return ConfirmDelete()'))}}
                                {{csrf_field()}}
                                <button type="submit" class="glyphicon glyphicon-trash"></button>
                                {{csrf_field()}}
                                {{Form::close()}}
                            </td>
                            <td width="3">
                                {{Form::open(array('url'=>route('print.show',$i->student_number) ,'method'=>'GET'))}}
                                {{csrf_field()}}
                                <button type="submit" class="glyphicon glyphicon-print"></button>
                                {{csrf_field()}}
                                {{Form::close()}}
                            </td>
                            <td width="3">
                                {{Form::open(array('url'=>route('otr.show',$i->student_number) ,'method'=>'GET'))}}
                                {{csrf_field()}}
                                <button type="submit" class="glyphicon glyphicon-user"></button>
                                {{csrf_field()}}
                                {{Form::close()}}
                            </td>
                        </tr>
						
                    @endforeach
                </table>

            </div>
        </div>
        <div class="row">
            <div class="col-md-2">&nbsp;</div>
            <div class="col-md-8" align="center">{{$info->links()}}</div>
        </div>

        <div class="uk-container uk-container-center" style="margin-top: 15px;">
            <div class="uk-grid">
                <div class="uk-width-1-1 uk-visible-large">
                    <div class="uk-border-rounded" style="background-color: #006e1b; padding: 8px; color: #FFF; margin-bottom: 25px;">
                        <img src="/images/mmsu.png" alt="" width="50">
                        &nbsp;
                        Copyright {{\Carbon\Carbon::now()->format('Y')}} by MMSU ITC and CHuMs
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection