<?php
    use App\User;
?>

@extends('layouts.app')
@section('content')
    <div class="container">
        @if(count($errors)>=1)
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-8">
                    @foreach($errors->all() as $c)
                        <span class="help-block">{{$c}}</span>
                    @endforeach
                </div>
            </div>
            <br>
        @endif

        
               <div class="row">
                   <div class="col-md-10">&nbsp;</div>
                   <div class="col-md-2">
						<div class="col-md-10">
						<button type="button" class="btn btn-primary " data-toggle="modal" data-target="#myModal">
                           Print
						</button>
						</div>
                       <!-- Modal -->
                       <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                           <div class="modal-dialog" role="document">
                               <div class="modal-content">
                                   <div class="modal-header">
                                       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                           <span aria-hidden="true">&times;</span>
                                       </button>
                                       <h4 class="modal-title" id="myModalLabel">Select Event to print</h4>
                                   </div>
                                   <div class="modal-body">

										{{Form::open(array('url'=>route('print.show',$info->student_number),'method'=>'GET'))}}
										{{csrf_field()}}

										{{Form::select('events_id',$e,null, ['class' => 'form-control'])}}


                                   </div>
                                   <div class="modal-footer">
                                       {{Form::submit('Print',array('class'=>'btn btn-warning'))}}
                                       {{csrf_field()}}
                                       {{Form::close()}}
                                       <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                   </div>
                               </div>
                           </div>
                       </div>

                   </div>
               </div>
                <br>
         
         <script>
            function ConfirmCreate()
            {
                var x = confirm("Are you sure of the information you entered?");
                if (x)
                    return true;
                else
                    return false;
            }
        </script>
        {{Form::model($info,array('url'=>route('form.update', Crypt::encrypt($info->student_number)),'method'=>'PATCH','files'=>'true', 'onsubmit' => 'return ConfirmCreate()' ))}}
        {{csrf_field()}}
        <div class="row">
            <div class="col-md-8">
                &nbsp;
            </div>
            <div class="col-md-2">&nbsp;</div>
            <div class="col-md-2">
                <div class="form-group">
                    <img src="{{$info->pic}}" class="img-responsive img-circle">
                    {{Form::label('Upload Image')}}
                    {{Form::file('image',null,array('class'=>'form-control'))}}
                </div>

            </div>
        </div>
        <br>
        <div class="row">
            <div class="form-group">
                <div align="center"><h1>Student Information</h1></div>
                        <br/>
                        <br/>
                <div class="col-md-2">{{Form::label('Student Number')}}</div>
                <div class="col-md-3">
                    {{Form::text('student_number',null,array('class'=>'form-control','readonly'))}}
                </div>
                <div class="col-md-1">
                    {{Form::label('SCUAA Category')}}
                </div>
                <div class="col-md-3">
                    {{Form::select('scuaaCategory', array(1 => 'Regional', 2 => 'National'), $info->scuaaCategory, array('class'=>'form-control'))}}
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="form-group">
                <div class="col-md-2">{!! Form::label('Name') !!}</div>
                <div class="col-md-3">
                    {{Form::text('lname',null,array('class'=>'form-control','placeholder'=>'Last Name'))}}
                </div>
                <div class="col-md-3">
                    {{Form::text('fname',null,array('class'=>'form-control','placeholder'=>'First Name'))}}
                </div>
                <div class="col-md-3">
                    {{Form::text('mname',null,array('class'=>'form-control','placeholder'=>'Middle Name'))}}
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="form-group">
                <div class="col-md-2">{{Form::label('Birth date')}}</div>
                <div class="col-md-2">
                    {{Form::date('dob', null,array('class'=>'form-control'))}}
                </div>
                <div class="col-md-1">{{Form::label('Age')}}</div>
                <div class="col-md-2">
                    {{ Form::text('age', null, array('class'=>'form-control', 'placeholder'=>'Age', 'disabled')) }}
                </div>
            </div>
        </div>
            <br>
                <div class="row">
                    <div class="form-group">
                        <div class="col-md-2">{{ Form::label('Password') }}</div>
                        <div class="col-md-2">
                            <input id="password" type="password" class="form-control" name="password" value="{{ $info->password }}" required>
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-2">
                        </div>
                    </div>
                </div>
        <br>
        <div class="row">
            <div class="form-group">
                <div class="col-md-2">{{Form::label('Address')}}</div>
                <div class="col-md-4">
                    {{Form::text('address',null,array('class'=>'form-control','placeholder'=>'Address'))}}
                </div>
                <div class="col-md-1">{{Form::label('Contact #')}}</div>
                <div class="col-md-4">
                    {{Form::text('contact',null,array('class'=>'form-control','placeholder'=>'Contact Number'))}}
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="form-group">
            <div align="center"><h1>Medical Information</h1></div>
                        <br/>
                        <br/>
                <div class="col-md-2">{{Form::label('Weight')}}</div>
                <div class="col-md-2">
                    {{Form::text('weight',null,array('class'=>'form-control','placeholder'=>'Weight'))}}
                </div>
                <div class="col-md-1">{{Form::label('Height')}}</div>
                <div class="col-md-2">
                    {{Form::text('height',null,array('class'=>'form-control','placeholder'=>'Height'))}}
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="form-group">
                <div class="col-md-2">{{Form::label('Blood Type')}}</div>
                <div class="col-md-1">
                    {{Form::text('blood_type',null,array('class'=>'form-control','placeholder'=>'Type'))}}
                </div>
                <div class="col-md-1">{{Form::label('Allergies')}}</div>
                <div class="col-md-3">
                    {{Form::text('allergies',null,array('class'=>'form-control','placeholder'=>'Allergies'))}}
                </div>
                <div class="col-md-1">{{Form::label('Medication if Any')}}</div>
                <div class="col-md-3">
                    {{Form::text('medication',null,array('class'=>'form-control','placeholder'=>'Medication'))}}
                </div>

            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-4">&nbsp;</div>
            <div class="col-md-6"><h1>Educational Background</h1></div>
        </div>
        <br>
        <div class="row">
            <div class="col=md-12">
                <table class="table">
                    <thead>
                    <td>&nbsp;</td>
                    <td>Name of School</td>
                    <td>Year Graduated</td>
                    </thead>
                    <tr>
                        <td>Elementary</td>
                        <td>{{Form::text('elementary[name]',$info->elem->first()!=null?$info->elem->first()->name:'',array('class'=>'form-control', 'placeholder'=>'Elementary School Name'))}}</td>
                        <td>{{Form::text('elementary[year]',$info->elem->first()!=null?$info->elem->first()->year_grad:'no',array('class'=>'form-control', 'placeholder'=>'Year Graduated (e.g. 2010)'))}}</td>
                    </tr>
                    <tr>
                        <td>High School</td>
                        <td>{{Form::text('high[name]',$info->high->first()!=null?$info->high->first()->name:'',array('class'=>'form-control', 'placeholder'=>'High School Name'))}}</td>
                        <td>{{Form::text('high[year]',$info->high->first()!=null?$info->high->first()->year_grad:'',array('class'=>'form-control', 'placeholder'=>'Year Graduated (e.g. 2014)'))}}</td>
                    </tr>
                </table>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col=md-12">
            <div align="center"><h1>Course Information</h1></div>
                <br/>
                <table class="table">
                    <tr>
                        <td>Course and Year</td>
                        <td>{{Form::text('col[name]',$info->col->first()!=null?$info->col->first()->name:'',array('class'=>'form-control', 'placeholder'=>'Course and Year (e.g. BSCS III)'))}}</td>
                        <td>{{Form::text('col[year]',$info->col->first()!=null?$info->col->first()->year_grad:'',array('class'=>'form-control', 'placeholder'=>'Year Graduated (e.g. 2018)'))}}</td>
                    </tr>
                    <tr>
                        <td>If Transfereee Specify</td>
                        <td>{{Form::text('trans[name]',$info->trans->first()!=null?$info->trans->first()->name:'',array('class'=>'form-control', 'placeholder'=>'Name of School Transferred From'))}}</td>
                        <td>{{Form::text('trans[year]',$info->trans->first()!=null?$info->trans->first()->year_grad:'',array('class'=>'form-control', 'placeholder'=>'Year Transferred (e.g. 2017)'))}}</td>
                    </tr>
                </table>
            </div>
        </div>
        
        <br />
        

        <div class="row">
            <div class="col-md-10">&nbsp;</div>
            <div class="col-md-2">
            {{Form::submit('Save Athlete Information', array('class'=>'btn btn-success'))}}
                <br />
                <br />
            </div>
        </div>
        <br />
        <script>
            function ConfirmDelete()
            {
                var x = confirm("Are you sure you want to delete?");
                if (x)
                    return true;
                else
                    return false;
            }
        </script>
        {{csrf_field()}}
        {{Form::close()}}
        {{Form::open(array('url'=>route('form.destroy', $info->student_number) ,'method'=>'DELETE', 'onsubmit' => 'return ConfirmDelete()'))}}
            {{csrf_field()}}
                
            {{csrf_field()}}
        {{Form::close()}}
		
	  <br />
	  <hr />	
      <div class="row">
          
          <div class="col-md-4" >&nbsp;</div>
		  <div class="col-md-4" align="center"><h2>Athlete Events</h2></div>
		  <div class="col-md-4">&nbsp;</div>
      </div>
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-striped table-responsive table-condensed">
                        <thead>
                        <tr>
                            <td>&nbsp;</td>
                            <td>Event</td>
                            <td>Status</td>
                            <td>&nbsp;</td>
                        </tr>
                        </thead>
                        <script>
                            function ConfirmDelete()
                            {
                                var x = confirm("Are you sure you want to delete?");
                                if (x)
                                    return true;
                                else
                                    return false;
                            }
                        </script>
                        <script>
                            function ConfirmCreate()
                            {
                                var x = confirm("Are you sure of the information you entered?");
                                if (x)
                                    return true;
                                else
                                    return false;
                            }
                        </script>
                        @if($info->events->count())
                            @foreach($info->events as $e)
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>{{$e->name->first()->desc}}</td>
                                    <td>{{Form::select('status',array(
                                    1=>'Active',2=>'In Active',3=>'Graduated',4=>'Transfered'
                                ),$e->status,array('class'=>'form-control','disabled'))}}
                                    </td>
                                    <td>
                                        {{Form::open(array('url'=>route('event.destroy',$e->id),'method'=>'DELETE', 'onsubmit' => 'return ConfirmDelete()') )}}
                                        {{csrf_field()}}
                                        {{Form::submit('Delete',array('class'=>'btn btn-danger'))}}
                                        {{csrf_field()}}
                                        {{Form::close()}}
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        <tr>
                            <td>&nbsp;</td>
                            {{Form::open(array('url'=>route('event.store'),'method'=>'POST', 'onsubmit' => 'return ConfirmCreate()'))}}
                            {{csrf_field()}}
                            <td>
                                {{Form::hidden('student_number',$info->student_number)}}
                                {{Form::select('events_id',$events->pluck('desc','id'),null, ['class' => 'form-control'])}}
                            </td>
                            <td>{{Form::select('status',array(1=>'Active',2=>'In Active',3=>'Graduated',4=>'Transfered'),null,array('class'=>'form-control'))}}
                            </td>
                            
                            <td>{{Form::submit('Add',array('class'=>'btn btn-success'))}}</td>
                            {{csrf_field()}}
                            {{Form::close()}}
                        </tr>
                    </table>
                </div>
            </div>
    
	<br />
	<hr />
	
	</div>


    <div class="row" >
        
		<div class="col-md-4" >&nbsp;</div>
		<div class="col-md-4" align="center"><h2>  Attachment</h2></div>
		<div class="col-md-4">&nbsp;</div>  
     </div>
            <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-striped table-responsive table-condensed">
                        <thead>
                        <tr>
                            <td>&nbsp;</td>
                            <td>Attachment Name</td>
                            <td>Attachment URL</td>
                            <td>&nbsp;</td>
                        </tr>
                        </thead>
                        <script>
                            function ConfirmDelete()
                            {
                                var x = confirm("Are you sure you want to delete?");
                                if (x)
                                    return true;
                                else
                                    return false;
                            }
                        </script>
                        @if($info->documents->count())
                            @foreach($info->documents as $docu)
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>
                                        {{$docu->attachment_name}}
                                    </td>
                                    <td>
                                        {{ $docu->attachment }}
                                    </td>
                                    <td width="3" align="right">
                                        {{Form::open(array('url'=>route('attachment.destroy', $docu->id),'method'=>'DELETE', 'onsubmit' => 'return ConfirmDelete()') )}}
                                        {{csrf_field()}}
                                            {{Form::submit('Delete',array('class'=>'btn btn-danger'))}}
                                            {{csrf_field()}}
                                        {{Form::close()}}
                                    </td>
                                    <td width="3">
                                        {{Form::open(array('url'=>route('attachment.show', $docu->id), 'method'=>'GET'))}}
                                        {{csrf_field()}}
                                            {{Form::submit('Download', array('class'=>'btn btn-success'))}}
                                            {{csrf_field()}}
                                        {{Form::close()}}
                                    </td>


                                </tr>
                            @endforeach
                        @endif
                        <tr>
                            <td>&nbsp;</td>
                            {{Form::open(array('url'=>route('attachment.store'), 'method'=>'POST', 'files'=>'true', 'onsubmit' => 'return ConfirmCreate()'))}}
                            {{csrf_field()}}
                            
                            <td>
                                {{Form::hidden('student_number', $info->student_number)}}
                                {{Form::text('attachment_name', null, array('class'=>'form-control', 'placeholder'=>'Document Name'))}}
                            </td>
                            <td>&nbsp;</td>
                            <td align="center">
                                {{Form::file('image', null, array('class'=>'form-control'))}}
                            </td>
                            
                            <td>{{Form::submit('Upload', array('class'=>'btn btn-success'))}}</td>
                            {{csrf_field()}}
                            {{Form::close()}}
                        </tr>
                    </table>
                </div>
            </div>
		<br />
		<hr />	
		</div>
	</div>
@endsection