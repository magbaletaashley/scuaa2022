@extends('layouts.app')
@section('content')
    <div class="container">
        @if(\Illuminate\Support\Facades\Session::has('messages'))
            <div class="row">
                <div class="col-md-2">&nbsp;</div>
                <div class="col-sm-8 alert alert-danger">
                    <center><span >{{\Illuminate\Support\Facades\Session::get('messages')}}</span></center>
                </div>
            </div>
        @endif

            <div class="row">
                {{Form::open(array('url'=>route('eve'),'method'=>'POST'))}}
                {{csrf_field()}}
                <div class="form-group"></div>
                {{csrf_field()}}
                {{Form::close()}}
                <div class="col-md-2">&nbsp; </div>
                {{Form::open(array('url'=>route('ser'),'method'=>'POST'))}}
                {{csrf_field()}}
                <div class="col-md-12" align="right">
                    <a href="{{ route('coach.create') }}" class='btn btn-primary'>Add Coach Information</a>
                </div>
                {{csrf_field()}}
                {{Form::close()}}
            </div>

        <div class="row">
            <div class="col-md-12">
                <table class="table table-responsive table-condensed">
                    <thead>

                    <td>Name</td>
                    <td>&nbsp;</td>
                    </thead>
                    @foreach($info as $i)
                        <tr>

                            <td><a href="{!! route('coach.edit',$i->id)!!}">{{$i->lname}},{{$i->fname}} {{$i->mname}}</a></td>
                            <td>
                                {{Form::open(array('url'=>route('coach.destroy',$i->id) ,'method'=>'DELETE'))}}
                                {{csrf_field()}}
                                <button type="submit" class="glyphicon glyphicon-trash"></button>
                                {{csrf_field()}}
                                {{Form::close()}}
                            </td>
                        </tr>
                    @endforeach
                </table>

            </div>
        </div>
        <div class="row">
            <div class="col-md-8">&nbsp;</div>
            <div class="col-md-4">{{ $info->links() }}</div>
        </div>
    </div>
@endsection