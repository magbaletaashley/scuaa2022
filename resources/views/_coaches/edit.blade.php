@extends('layouts.app')
@section('content')

    <div class="container">
        @if(count($errors)>=1)
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-8">
                    @foreach($errors->all() as $c)
                        <span class="help-block">{{$c}}</span>
                    @endforeach
                </div>
            </div>
            <br>
        @endif
        {{Form::model($info,array('url'=>route('coach.store'),'method'=>'POST','files'=>'true' ))}}
        {{csrf_field()}}
        <div class="row">
            <div class="col-md-8">
                &nbsp;
            </div>
            <div class="col-md-2">&nbsp;</div>
            <div class="col-md-2">
                <div class="form-group">
                    {{Form::label('Upload Image')}}
                    {{Form::file('image',null,array('class'=>'form-control'))}}
                </div>

            </div>
        </div>
        <br>
        <div class="row">
            <div class="form-group">
                <div class="col-md-2">{{Form::label('Name')}}</div>
                <div class="col-md-3">
                    {{Form::text('lname',null,array('class'=>'form-control','placeholder'=>'Last Name'))}}
                </div>
                <div class="col-md-3">
                    {{Form::text('fname',null,array('class'=>'form-control','placeholder'=>'First Name'))}}
                </div>
                <div class="col-md-3">
                    {{Form::text('mname',null,array('class'=>'form-control','placeholder'=>'Middle Name'))}}
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="form-group">
                <div class="col-md-2">{{Form::label('Birth date')}}</div>
                <div class="col-md-2">
                    {{Form::date('bday', \Carbon\Carbon::now(),array('class'=>'form-control'))}}
                </div>
                <div class="col-md-1">{{Form::label('Age')}}</div>
                <div class="col-md-2">
                    {{Form::text('age',null,array('class'=>'form-control','placeholder'=>'Age'))}}
                </div>
                <div class="col-md-1">{{Form::label('Email')}}</div>
                <div class="col-md-2">
                    {{Form::text('email',null,array('class'=>'form-control','placeholder'=>'Email Address'))}}
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="form-group">
                <div class="col-md-2">{{Form::label('Address')}}</div>
                <div class="col-md-4">
                    {{Form::text('address',null,array('class'=>'form-control','placeholder'=>'Adress'))}}
                </div>
                <div class="col-md-1">{{Form::label('Contact #')}}</div>
                <div class="col-md-4">
                    {{Form::text('contact',null,array('class'=>'form-control','placeholder'=>'Contact Number'))}
                </div>
            </div>
        </div>
        <br>
        <br>
        <div class="row">
            <div class="col-md-10">&nbsp;</div>
            <div class="col-md-2">
                {{Form::submit('Save',array('class'=>'btn btn-success'))}}
            </div>
        </div>
        {{csrf_field()}}
        {{Form::close()}}
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-striped table-responsive table-condensed">
                        <thead>
                        <tr>
                            <td>&nbsp;</td>
                            <td>Event</td>
                            <td>Status</td>
                            <td>&nbsp;</td>
                        </tr>
                        </thead>

                          @if($info->events->count())
                             @foreach($info->events as $e)
                                <tr>
                                     <td>&nbsp;</td>
                                     <td>{{$e->name->desc}}</td>
                                      <td>{{Form::select('status',array(
                                        1=>'Coach',2=>'Asst. Coach',3=>'Orbit'
                                            ),$e->status,array('class'=>'form-control','disabled'))}}
                                      </td>
                                     <td>&nbsp;</td>
                                    <td>
                                        {{Form::open(array('url'=>route('ce.destroy',$e->id),'method'=>'DELETE') )}}
                                        {{csrf_field()}}

                                        {{Form::submit('Delete',array('class'=>'btn btn-danger'))}}
                                        {{csrf_field()}}
                                        {{Form::close()}}
                                    </td>
                                </tr>
                             @endforeach
                           @endif

                        <tr>
                            <td>&nbsp;</td>
                            {{Form::open(array('url'=>route('ce.store'),'method'=>'POST'))}}
                            {{csrf_field()}}
                            <td>
                                {{Form::hidden('coach_id',$info->id)}}
                                {{Form::select('event',$events->pluck('desc','id'),null, ['class' => 'form-control'])}}
                            </td>
                            <td>{{Form::select('status',array(
                                    1=>'Coach',2=>'Asst. Coach',3=>'Orbit'
                                ),null,array('class'=>'form-control'))}}
                            </td>

                            <td>{{Form::submit('Add',array('class'=>'btn btn-success'))}}</td>
                            {{csrf_field()}}
                            {{Form::close()}}
                        </tr>

                    </table>
                </div>
            </div>
    </div>
@endsection