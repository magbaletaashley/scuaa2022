@extends('layouts.app')
@section('content')

    <div class="container">
        @if(count($errors)>=1)
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-8">
                    @foreach($errors->all() as $e)
                        <span class="help-block">{{$e}}</span>
                    @endforeach
                </div>
            </div>
            <br>
        @endif
        {{Form::open(array('url'=>route('coach.store'),'method'=>'POST','files'=>'true' ))}}
        {{csrf_field()}}
        <div class="row">
            <div class="col-md-8">
                &nbsp;
            </div>
            <div class="col-md-2">&nbsp;</div>
            <div class="col-md-2">
                <div class="form-group">
                    {{Form::label('Upload Image')}}
                    {{Form::file('image',null,array('class'=>'form-control'))}}
                </div>

            </div>
        </div>
        <br>
        <div class="row">
            <div class="form-group">
                <div class="col-md-2">{{Form::label('Name')}}</div>
                <div class="col-md-3">
                    {{Form::text('lname',null,array('class'=>'form-control','placeholder'=>'Last Name'))}}
                </div>
                <div class="col-md-3">
                    {{Form::text('fname',null,array('class'=>'form-control','placeholder'=>'First Name'))}}
                </div>
                <div class="col-md-3">
                    {{Form::text('mname',null,array('class'=>'form-control','placeholder'=>'Middle Name'))}}
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="form-group">
                <div class="col-md-2">{{Form::label('Birth date')}}</div>
                <div class="col-md-2">
                    {{Form::date('bday', \Carbon\Carbon::now(),array('class'=>'form-control'))}
                </div>
                <div class="col-md-1">{{Form::label('Age')}}</div>
                <div class="col-md-2">
                    {{Form::text('age',null,array('class'=>'form-control','placeholder'=>'Age'))}}
                </div>
                <div class="col-md-1">{{Form::label('Email')}}</div>
                <div class="col-md-2">
                    {{Form::text('email',null,array('class'=>'form-control','placeholder'=>'Email Address'))}}
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="form-group">
                <div class="col-md-2">{{Form::label('Address')}}</div>
                <div class="col-md-4">
                    {{Form::text('address',null,array('class'=>'form-control','placeholder'=>'Adress'))}}
                </div>
                <div class="col-md-1">{{Form::label('Contact #')}}</div>
                <div class="col-md-4">
                    {{Form::text('contact',null,array('class'=>'form-control','placeholder'=>'Contact Number'))}}
                </div>
            </div>
        </div>
        <br>
       <br>
        <div class="row">
            <div class="col-md-10">&nbsp;</div>
            <div class="col-md-2">
                {{Form::submit('Save',array('class'=>'btn btn-success'))}}
            </div>
        </div>
        {{csrf_field()}}
        {{Form::close()}}
    </div>

@endsection