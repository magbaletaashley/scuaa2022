<style>
    @media all {
        .page-break  { display: none; }
        td{
            vert-align: top;
        }
    }

    @media print {
        td{
            vert-align: top;
        }
        .page-break  { display: block; page-break-before: always; }
    }

    .my-table {
        page-break-before: always;
        page-break-after: always;
    }
    .my-table tr {
        page-break-inside: avoid;
    }
    #c{
        border:solid .5px;
    }

</style>






        {{--*/ $j = 1 /*--}}


<table width="100%" border="0" id="gal" cellspacing="0" cellpadding="0">
    <thead>
    <tr >
        <td  colspan="2" align="left" >
            <div ><h2>SCUAA-I OLYMPICS 2022 </h2></div>
            <div style="border-bottom: solid 1px;"><h4>Regional: Pangasinan State University (PSU) - November 21-25, 2022 </h4></div>
            <br>
            <div>
                <table  width="100%">

                    <tr>
                        <td width="10%"><h4>Region:</h4></td>
                        <td><div style="border-bottom: solid 1px; "align="center">I</div></td>
                    </tr>
                </table>

            </div>
        </td>
        <td  align="left"><img src="{{asset('images/scuaa.jpg')}}" style="width: 1.5in; height: 1.5in;" data-holder-rendered="true"></td>

        <td  colspan="6">
            <div align="right" ><div style="border: solid 1px;width: 30%">SCUAA FORM 2</div></div>
            <div style="border: solid 2px;"><h3><center>OFFICIAL ENTRY FORM AND GALLERY FOR</center> <center>{{$e->desc}}</center></h3></div>
            <div >
                <table  width="100%">

                    <tr>
                        <td width="30%"><h4>Date Of Screening:</h4></td>
                        <td><div style="border-bottom: solid 1px; "align="center">November, 2022</div></td>
                    </tr>
                </table>
            </div>
            <div>
                <table  width="100%">

                    <tr>
                        <td width="30%"><h4>Category:</h4></td>
                        <td><div style="border-bottom: solid 1px; "align="center"> <center>{{$e->desc}}</center></div></td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
    <tr>
        <td id="c" width="40%"><center>NAME OF ATHLETE</center></td>
        <td id="c" width="5%"><center>Age</center></td>
        <td id="c" width="15%"><center>Date of Birth</center></td>
        <td id="c" width="5%"><center>FORM 2</center></td>
        <td id="c" width="5%"><center>FORM 3</center></td>
        <td id="c"  width="2%" ><center>TOR</center></td>
        <td id="c"  width="2%"><center>NSO Birth Certificate</center></td>
        <td id="c"  width="35%"><center>Remarks</center></td>
    </tr>

    </thead>

    <tbody>


    <?php $j=1; $count=1;?>
      @foreach($players->chunk(18) as $player)

        @foreach($player as $p)
            <tr>
                <td id="c" width="20%">{{$count}}. {{strtoupper($p->lname.",".$p->fname." ".substr($p->mname,0,1).".")}}</td>
                <td id="c" width="5%"><center>{{$p->age}}</center></td>
                <td id="c" width="5%"><center>{{\Carbon\Carbon::parse($p->dob)->format("M-d-Y")}}</center></td>
                <td id="c" width="5%"><center>&nbsp;</center></td>
                <td id="c" width="5%"><center>&nbsp;</center></td>
                <td id="c"  width="5%" ><center>&nbsp;</center></td>
                <td id="c"  width="5%"><center>&nbsp;</center></td>
                <td id="c"  width="20%"><center>&nbsp;</center></td>
            </tr>
            <?php $j=$loop->index+1; $count++?>
        @endforeach
        @if($j<=17)
        @for($i=$j+1;$i<=18;$i++)
            <tr>

                <td id="c">&nbsp;</td>
                <td id="c">&nbsp;</td>
                <td id="c">&nbsp;</td>
                <td id="c">&nbsp;</td>
                <td id="c">&nbsp;</td>
                <td id="c">&nbsp;</td>
                <td id="c">&nbsp;</td>
                <td id="c">&nbsp;</td>
            </tr>
            <?php  $count++?>
       @endfor
    @endif

    <tr>
        <td colspan="1" id="c">
            <div>&nbsp;</div>
            <div>&nbsp;</div>
            <div style="border-bottom: solid 0.5px" align="center">
                &nbsp;
            </div>
            <div align="center">Name & Signature of Coach</div>
        </td>
        <td colspan="4" id="c">
            <div>&nbsp;</div>
            <div>&nbsp;</div>
            <div style="border-bottom: solid 0.5px" align="center">
                <b>ROWELL C. TAGATAC</b>
            </div>
            <div align="center">Name & Signature Regional Sports Director</div>
        </td>
        <td  id="c" colspan="4">&nbsp;</td>
    </tr>

    @endforeach
    </tbody>

</table>












