<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {!! Html::style('css/bootstrap.min.css')!!}
    <style type="text/css" media="print">

        a:link:after, a:visited:after {
            content: normal;
        }
        a:link:after, a:visited:after {
            content: normal !important;
        }
        @page {
            size: auto;   /* auto is the initial value */
            margin: 0;  /* this affects the margin in the printer settings */
            margin-left: 1cm;
            margin-right: 1cm;
        }

    </style>
</head>
<body>
<div class="container">
    @yield('content')
</div>

</body>
</html>