<?php
    use App\User;
    use Illuminate\Support\Facades\Auth;
?>

<html>
<head>
    {!! Html::style('css/bootstrap.css')!!}
            <!--<style type="text/css" rel="css/bootstrap.min.css"></style>
     Latest compiled and minified JavaScript -->
     
</head>
<body>
<nav class="navbar navbar-default ">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div class="navbar-header" align="center">
                <a class="navbar-brand">SCUAA-I Athletes Information System |</a>
            </div>
            <ul class="nav navbar-nav navbar-right">
                @if(Auth::check() && (User::where('id', Auth::id())->where('userType', 'admin')->count() != 0))
                    <li><a href="{!! route('form.index') !!}"><span class="glyphicon glyphicon-home"></span> Home</a></li>
                @elseif(Auth::check())
                    <li><a href="{!! route('form.index') !!}"><span class="glyphicon glyphicon-home"></span> Profile</a></li>
                @elseif(User::where('id', Auth::id())->where('userType', 'coach')->count() != 0)
                    <li><a href="{!! route('form.index') !!}"><span class="glyphicon glyphicon-home"></span> Profile</a></li>
                @endif
                @if(Auth::check())
                @else
                    <li><a href="{!! route('form.create') !!}"><span class="glyphicon glyphicon-list-alt"></span> Athlete Information Registration</a></li>
                @endif
                @if(Auth::check() && (User::where('id', Auth::id())->where('userType', 'admin')->count() != 0) || User::where('id', Auth::id())->where('userType', 'coach')->count() != 0)
                    @if(User::where('id', Auth::id())->where('userType', 'coach')->count() != 0)
                    @else
                        <li><a href="{!! route('coach.index') !!}"><span class="glyphicon glyphicon-cog"></span> Coach Information</a></li>
						
						<li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            Printables<span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{!! route('masterlist.index') !!}"><span class="glyphicon glyphicon-list"></span> Master List</a></li>
                            
                            <li><a href="{!! route('gallery.index') !!}"><span class="glyphicon glyphicon-user"></span> Event Gallery</a></li>
                            
                            <li><a href="{!! route('cl.index') !!}"><span class="glyphicon glyphicon-list-alt"></span> Check List</a></li>
                        </ul>
                    </li>
					
					@endif
                                        
					@if(User::where('id', Auth::id())->where('userType', 'coach')->count() != 0)
					
					@else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            Administrator Functions<span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            
                                <li><a href="{{ url('/addAdmin') }}"><span class="glyphicon glyphicon-list-alt"></span> Add Admin</a></li>
                            
                                <li><a href="{{ url('/adminList') }}"><span class="glyphicon glyphicon-list-alt"></span> Admin List</a></li>

                                <li><a href="{!! url('/unconfirmed') !!}"><span class="glyphicon glyphicon-list"></span> Unconfirmed List</a></li>
                            
                        </ul>
                    </li>
					@endif
				@endif
                @if(Auth::check())
                    <li>
                        <a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <span class="glyphicon glyphicon-log-out"></span> Logout
                        </a>
                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                            {{csrf_field()}}
                            {{csrf_field()}}
                        </form>
                    </li>
                @else
                    <li><a href="{!! route('login') !!}"><span class="glyphicon glyphicon-user"></span> Log In</a></li>
                @endif
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
@yield('content')
{!! Html::script('js/jquery.js')!!}
{!! Html::script('js/bootstrap.js')!!}

<script>
    $(function(){
        $('#dob').change(function(){

            var bdate = new Date($(this).val()).getFullYear();
            var dateToday = new Date('{{ \Carbon\Carbon::today()->format('Y-m-d') }}').getFullYear();

            $('#age').val(parseInt(dateToday) - parseInt(bdate));
        });
    })
</script>

<script>
    $(function(){
        $('#bday').change(function(){

            var bdate = new Date($(this).val()).getFullYear();
            var dateToday = new Date('{{ \Carbon\Carbon::today()->format('Y-m-d') }}').getFullYear();

            $('#age').val(parseInt(dateToday) - parseInt(bdate));
        });
    })
</script>

</body>
</html>


