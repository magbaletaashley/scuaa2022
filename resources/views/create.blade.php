@extends('layouts.app')
@section('content')

    <div class="container">
        @if(count($errors)>=1)
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-8">
                    @foreach($errors->all() as $e)
                        <span class="help-block">{{$e}}</span>
                    @endforeach
                </div>
            </div>
            <br>
        @endif
        <script>
            function ConfirmCreate()
            {
                var x = confirm("Are you sure of the information you entered?");
                if (x)
                    return true;
                else
                    return false;
            }
        </script>
        {{ Form::open(array('url'=>route('form.store'), 'method'=>'POST', 'files'=>'true', 'onsubmit' => 'return ConfirmCreate()')) }}
        {{ csrf_field() }}
            <div class="row">
                <div class="col-md-8">
                    &nbsp;
                </div>
                <div class="col-md-2">&nbsp;</div>
                <div class="col-md-2">
                    <div class="form-group">
                        {{ Form::label('Upload Image') }}
                        {{ Form::file('image', null, array('class'=>'form-control')) }} 
                    </div>
                </div>
            </div>
            <br>
                <div class="row">
                    <div class="form-group">
                        <div align="center"><h1>Student Information</h1></div>
                        <br/>
                        <br/>
                        <div class="col-md-2">{{ Form::label('Student Number') }}</div>
                        <div class="col-md-3">
                            {{ Form::text('student_number',null,array('class'=>'form-control', 'placeholder'=>'Student Number')) }}
                        </div>
                        <div class="col-md-1">
                            {{ Form::label('SCUAA Category') }}
                        </div>
                        <div class="col-md-3">
                            {{ Form::select('scuaaCategory', array(1 => 'Regional', 2 => 'National'), null, array('class'=>'form-control')) }}
                        </div>
                      <!--  <div class="col-md-1">{!! Form::label('Event') !!}</div>
                        <div class="col-md-3">
                            {!! Form::select('event',$events->pluck('desc','id'), null, ['class' => 'form-control']) !!}
                        </div>-->
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="form-group">
                        <div class="col-md-2">{{ Form::label('Name') }}</div>
                        <div class="col-md-3">
                            {{ Form::text('lname', null, array('class'=>'form-control', 'placeholder'=>'Last Name')) }}
                        </div>
                        <div class="col-md-3">
                            {{ Form::text('fname', null, array('class'=>'form-control', 'placeholder'=>'First Name')) }}
                        </div>
                        <div class="col-md-3">
                            {{ Form::text('mname', null, array('class'=>'form-control', 'placeholder'=>'Middle Name')) }}
                        </div>
                    </div>
                </div>
                <br>
            <div class="row">
                <div class="form-group">
                    <div class="col-md-2">{{ Form::label('Birth date') }}</div>
                    <div class="col-md-2">
                        {{ Form::date('dob', \Carbon\Carbon::now(), array('class'=>'form-control')) }}
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-2">
                        
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="form-group">
                    <div class="col-md-2">{{ Form::label('Password') }}</div>
                    <div class="col-md-2">
                        <input id="password" type="password" class="form-control" name="password" required>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-2">
                        
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="form-group">
                    <div class="col-md-2">{{ Form::label('Address') }}</div>
                    <div class="col-md-4">
                        {{ Form::text('address',null,array('class'=>'form-control','placeholder'=>'Address')) }}
                    </div>
                    <div class="col-md-1">{{ Form::label('Contact #') }}</div>
                    <div class="col-md-4">
                        {{ Form::text('contact', null, array('class'=>'form-control', 'placeholder'=>'Contact Number')) }}
                    </div>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="form-group">
                    <div align="center"><h1>Medical Information</h1></div>
                    <br/>
                    <br/>
                    <div class="col-md-2">{{ Form::label('Weight') }}</div>
                    <div class="col-md-2">
                        {{ Form::text('weight', null, array('class'=>'form-control', 'placeholder'=>'Weight')) }}
                    </div>
                    <div class="col-md-1">{{ Form::label('Height') }}</div>
                    <div class="col-md-2">
                        {{ Form::text('height', null, array('class'=>'form-control', 'placeholder'=>'Height')) }}
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="form-group">
                    <div class="col-md-2">{{ Form::label('Blood Type') }}</div>
                    <div class="col-md-1">
                        {{ Form::text('blood_type', null, array('class'=>'form-control', 'placeholder'=>'Type')) }}
                    </div>
                    <div class="col-md-1">{{Form::label('Allergies')}}</div>
                    <div class="col-md-3">
                        {{Form::text('allergies',null,array('class'=>'form-control','placeholder'=>'Allergies'))}}
                    </div>
                    <div class="col-md-1">{{Form::label('Medication if Any')}}</div>
                    <div class="col-md-3">
                        {{Form::text('medication',null,array('class'=>'form-control','placeholder'=>'Medication'))}}
                    </div>
                </div>
            </div>
        <br>
        <br>
        <div class="row">
           <div class="col-md-4">&nbsp;</div>
            <div class="col-md-6"><h1>Educational Background</h1></div>
        </div>
        <br>
        <div class="row">
            <div class="col=md-12">
                <table class="table">
                    <thead>
                        <td>&nbsp;</td>
                        <td>Name of School</td>
                        <td>Year Graduated</td>
                    </thead>
                   <tr>
                       <td>Elementary</td>
                       <td>{{Form::text('elementary[name]',null,array('class'=>'form-control', 'placeholder'=>'Elementary School Name'))}}</td>
                       <td>{{Form::text('elementary[year]',null,array('class'=>'form-control', 'placeholder'=>'Year Graduated (e.g. 2010)'))}}</td>
                   </tr>
                    <tr>
                        <td>High School</td>
                        <td>{{Form::text('high[name]',null,array('class'=>'form-control', 'placeholder'=>'High School Name'))}}</td>
                        <td>{{Form::text('high[year]',null,array('class'=>'form-control', 'placeholder'=>'Year Graduated (e.g. 2014)'))}}</td>
                    </tr>
                </table>
            </div>
        </div>
        <br>
        <br>
        <div class="row">
            <div class="col=md-12">
                <div align="center"><h1>Course Information</h1></div>
                <br/>
                <table class="table">
                    <tr>
                        <td>Course and Year</td>
                        <td>{{Form::text('col[name]',null,array('class'=>'form-control', 'placeholder'=>'Course and Year (e.g. BSCS III)'))}}</td>
                        <td>{{Form::text('col[year]',null,array('class'=>'form-control', 'placeholder'=>'Year Graduated (e.g. 2018)'))}}</td>
                    </tr>
                    <tr>
                        <td>If Transfereee Specify</td>
                        <td>{{Form::text('trans[name]',null,array('class'=>'form-control', 'placeholder'=>'Name of School Transferred From'))}}</td>
                        <td>{{Form::text('trans[year]',null,array('class'=>'form-control' , 'placeholder'=>'Year Transferred (e.g. 2017)'))}}</td>
                    </tr>
                </table>
            </div>
        </div>
        <br>

        
        <div class="row">
            <div class="col-md-10">&nbsp;</div>
            <div class="col-md-2">
                {{Form::submit('Save', array('class' =>' btn btn-success'))}}
            </div>
        </div>
        {{csrf_field()}}
        {{Form::close()}}
    </div>
@endsection