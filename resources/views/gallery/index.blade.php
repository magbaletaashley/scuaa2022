@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="col-md-12"><br><br><br><br><br><br></div>
        <div class="col-md-4"><br><br><br><br><br><br></div>
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading"><h2>Print Gallery</h2></div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                {{Form::open(array('url'=>route('gallery.store'), 'METHOD'=>'POST'))}}
                                {{csrf_field()}}
                                    {{Form::select('event', $events->pluck('desc','id'), null, array('class'=>'form-control'))}}
                                    <br />
                                    {{Form::select('scuaaCategory', array(1 => 'Regional', 2 => 'National'), null, array('class'=>'form-control'))}}
                                    <br />
                                    {{Form::submit('Print Gallery', array('class'=>'btn btn-success'))}}
                                {{csrf_field()}}
                                {{Form::close()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">&nbsp;</div>
    </div>
@endsection