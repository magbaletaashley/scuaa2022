<style>
    @media all {
        .page-break  { display: none; }
        td{
            vert-align: top;
        }
    }

    @media print {
        td{
            vert-align: top;
        }
        .page-break  { display: block; page-break-before: always; }
    }
   /* body {
        margin: 5px 5px 5px 5px;
        text-align:left;
        font-size: 11px;
    }
    table{
        border-collapse:collapse;
    }
    table tbody tr td{
        padding: 2px;;
    }
    ul {
        list-style-type:none;
    }
    li {
        list-style-type:none;
    }
    .page-break {
        page-break-before: always;

    }

    #gal{
        border-style: solid;
        border-collapse: collapse;
    }
    #gal th,td{
        padding:0;
    }
    .header{
        padding-top: 15px;
    }*/
    .my-table {
        page-break-before: always;
        page-break-after: always;
    }
    .my-table tr {
        page-break-inside: avoid;
    }
    #c{
        border:solid ;

    }
    table #cl{
        font-size:12px;
    }

</style>






        {{--*/ $j = 1 /*--}}


<table width="100%" border="0" id="gal" cellspacing="0" cellpadding="0">
    <thead >
    <tr >
        <td  colspan="3" align="left" >
            <div ><h2>SCUAA-I OLYMPICS 2022</h2></div>
            <div style="border-bottom: solid 1px;"><h4>Regional: Pangasinan State University (PSU) - November 21-25, 2022</h4></div>
            <br>
            <div>
                <table  width="100%">

                    <tr>
                        <td width="10%"><h4>Region:</h4></td>
                        <td><div style="border-bottom: solid 1px; "align="center">I</div></td>
                    </tr>
                </table>

            </div>
        </td>
        <td  align="left"><img src="{{asset('images/scuaa.jpg')}}" style="width: 1.5in; height: 1.5in;" data-holder-rendered="true"></td>

        <td  colspan="3">
            <div align="right" ><div style="border: solid 1px;width: 30%">SCUAA FORM 2</div></div>
            <div style="border: solid 2px;"><h3><center>OFFICIAL ENTRY FORM AND GALLERY FOR</center> <center>{{$e->desc}}</center></h3></div>
            <table  width="100%">
                <tr>
                    <td width="30%"><h4>Date Of Screening:</h4></td>
                    <td><div style="border-bottom: solid 1px; "align="center">November, 2022</div></td>
                </tr>
            </table>
            <table  width="100%">

                <tr>
                    <td width="30%"><h4>Category:</h4></td>
                    <td><div style="border-bottom: solid 1px; "align="center"> <center>{{$e->desc}}</center></div></td>
                </tr>
            </table>
        </td>
    </tr>
    </thead>





        <tbody>



                @foreach($players->chunk(6) as $player)

                <tr >
                    <td align="center" valign="top"  id="c">
                        <div align="center" ><b>&nbsp;</b></div>
                        <div align="center" style="border: solid 0px; width: 2in;height: 2in">
                            <img src="/images/mmsu.png" style="width: 1.5in;height: 1.5in">
                        </div>
                        <div>
                            <table border="1" style="width:100%;">
                                <tr>
                                    <td id="cl">Name</td>
                                </tr>
                                <tr>
                                    <td id="cl">Date of Birth</td>
                                </tr>
                                <tr>
                                    <td id="cl">Course & Year</td>
                                </tr>
                                <tr>
                                    <td id="cl">School</td>
                                </tr>
                            </table>
                        </div>
                    </td>


                    @foreach($player as $p)
                            <td align="center" valign="top"  id="c">
                                <div align="center" ><b>ATHLETE</b></div>
                                <div align="center" style="border: solid 1px; width: 2in;height: 2in">
										
                                        <img src="{{$p->pic}}" style="width: 2in;height: 2in">

                                </div>
                                <div>
                                    <table border="1" style="width:100%;">
                                        <tr>

                                                <td id="cl">{{strtoupper($p->lname.",".$p->fname." ".substr($p->mname,0,1).".")}}</td>

                                        </tr>
                                        <tr>

                                                <td id="cl">{{$p->dob}} </td>

                                        </tr>
                                        <tr>
                                                <td id="cl">
                                                    @if($p->col->count())
                                                        {{$p->col->first()['name']}}
                                                    @else
                                                        &nbsp;
                                                    @endif

                                                </td>

                                        </tr>
                                        <tr>


                                                <td id="cl">Mariano Marcos State University</td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        <?php $j=$loop->index+1?>
                    @endforeach

                    @if($j<=5)
                        @for($i=$j;$i<=5;$i++)
                            <td align="center" valign="top"  id="c">
                                <div align="center" ><b>ATHLETE</b></div>
                                <div align="center" style="border: solid 1px; width: 2in;height: 2in">
                                    &nbsp;
                                </div>
                                <div>
                                    <table border="1" style="width:100%;">
                                        <tr>

                                            <td id="cl">&nbsp;</td>

                                        </tr>
                                        <tr>

                                            <td id="cl">&nbsp;</td>

                                        </tr>
                                        <tr>

                                            <td id="cl">&nbsp;</td>

                                        </tr>
                                        <tr>

                                            <td id="cl">&nbsp;</td>

                                        </tr>
                                    </table>
                                </div>
                            </td>
                        @endfor
                    @endif
                    </tr>

                @endforeach

        <tr>
            <td align="center" valign="top"  id="c">
                <div align="center" ><b>&nbsp;</b></div>
                <div align="center" style="border: solid 0px; width: 2in;height: 2in" >
                    <img src="/images/mmsu.png" style="width: 1.5in;height: 1.5in">
                </div>
                <div>
                    <table border="1" style="width:100%;">
                        <tr>
                            <td id="cl">Name</td>
                        </tr>
                        <tr>
                            <td id="cl">Date of Birth</td>
                        </tr>
                        <tr>
                            <td id="cl">Contact No.</td>
                        </tr>
                        <tr>
                            <td id="cl">Email</td>
                        </tr>
                    </table>
                </div>
            </td>
            <td align="center" valign="top"  id="c">
                <div align="center" ><b>COACH</b></div>
                <div align="center" style="border: solid 1px; width: 2in;height: 2in">
                    @if($hc->count())
                        <img src="{{$hc->first()->info->pic}}" style="width: 2in;height: 2in">
                    @endif
                </div>
                <div>
                    <table border="1" style="width:100%;">
                        <tr>
                            @if($hc->count())
                                <td id="cl">{{strtoupper($hc->first()->info->fname)}} {{strtoupper(substr($hc->first()->info->mname,0,1))}}. {{strtoupper($hc->first()->info->lname)}}</td>
                            @else
                                <td id="cl">&nbsp;</td>
                            @endif
                        </tr>
                        <tr>
                            @if($hc->count())
                                <td id="cl">{{$hc->first()->info->bday}} </td>
                            @else
                                <td id="cl">&nbsp;</td>
                            @endif
                        </tr>
                        <tr>
                            @if($hc->count())
                                <td id="cl">{{$hc->first()->info->contact!=null?$hc->first()->info->contact:'&nbsp;'}} </td>
                            @else
                                <td id="cl">&nbsp;</td>
                            @endif
                        </tr>
                        <tr>
                            @if($hc->count())
                                <td id="cl">{{$hc->first()->info->email!=null?$hc->first()->info->email:"&nbsp;"}} </td>
                            @else
                                <td id="cl">&nbsp;</td>
                            @endif
                        </tr>
                    </table>
                </div>
            </td>
            <td align="center" valign="top"  id="c">
                <div align="center" ><b>COACH</b></div>
                <div align="center" style="border: solid 1px; width: 2in;height: 2in">
                    @if($ac->count())
                        <img src="{{$ac->first()->info->pic}}" style="width: 2in;height: 2in">
                    @endif
                </div>
                <div>
                    <table border="1" style="width:100%;">
                        <tr>
                            @if($ac->count())
                                <td id="cl">{{strtoupper($ac->first()->info->fname)}} {{strtoupper(substr($ac->first()->info->mname,0,1))}}. {{strtoupper($ac->first()->info->lname)}}</td>
                            @else
                                <td id="cl">&nbsp;</td>
                            @endif
                        </tr>
                        <tr>
                            @if($ac->count())
                                <td id="cl">{{$ac->first()->info->bday}} </td>
                            @else
                                <td id="cl">&nbsp;</td>
                            @endif
                        </tr>
                        <tr>
                            @if($ac->count())
                                <td id="cl">{{$ac->first()->info->contact!=null?$ac->first()->info->contact:'&nbsp;'}} </td>
                            @else
                                <td id="cl">&nbsp;</td>
                            @endif
                        </tr>
                        <tr>
                            @if($ac->count())
                                <td id="cl">{{$ac->first()->info->email!=null?$ac->first()->info->email:"&nbsp;"}} </td>
                            @else
                                <td id="cl">&nbsp;</td>
                            @endif
                        </tr>
                    </table>
                </div>
            </td>
            <td align="center" valign="top"  id="c">
                <div align="center" ><b>COACH</b></div>
                <div align="center" style="border: solid 1px; width: 2in;height: 2in">
                    @if($cc1->count())
                        <img src="{{$cc1->first()->info->pic}}" style="width: 2in;height: 2in">
                    @endif
                </div>
                <div>
                    <table border="1" style="width:100%;">
                        <tr>
                            @if($cc1->count())
                                <td id="cl">{{strtoupper($cc1->first()->info->fname)}} {{strtoupper(substr($cc1->first()->info->mname,0,1))}}. {{strtoupper($cc1->first()->info->lname)}}</td>
                            @else
                                <td id="cl">&nbsp;</td>
                            @endif
                        </tr>
                        <tr>
                            @if($cc1->count())
                                <td id="cl">{{$cc1->first()->info->bday}} </td>
                            @else
                                <td id="cl">&nbsp;</td>
                            @endif
                        </tr>
                        <tr>
                            @if($cc1->count())
                                <td id="cl">{{$cc1->first()->info->contact!=null?$cc1->first()->info->contact:'&nbsp;'}} </td>
                            @else
                                <td id="cl">&nbsp;</td>
                            @endif
                        </tr>
                        <tr>
                            @if($cc1->count())
                                <td id="cl">{{$cc1->first()->info->email!=null?$cc1->first()->info->email:"&nbsp;"}} </td>
                            @else
                                <td id="cl">&nbsp;</td>
                            @endif
                        </tr>
                    </table>
                </div>
            </td>
            <td align="center" valign="top"  id="c">
                <div align="center" ><b>COACH</b></div>
                <div align="center" style="border: solid 1px; width: 2in;height: 2in">
                    @if($cc2->count())
                        <img src="{{$cc2->first()->info->pic}}" style="width: 2in;height: 2in">
                    @endif
                </div>
                <div>
                    <table border="1" style="width:100%;">
                        <tr>
                            @if($cc2->count())
                                <td id="cl">{{strtoupper($cc2->first()->info->fname)}} {{strtoupper(substr($cc2->first()->info->mname,0,1))}}. {{strtoupper($cc2->first()->info->lname)}}</td>
                            @else
                                <td id="cl">&nbsp;</td>
                            @endif
                        </tr>
                        <tr>
                            @if($cc2->count())
                                <td id="cl">{{$cc2->first()->info->bday}} </td>
                            @else
                                <td id="cl">&nbsp;</td>
                            @endif
                        </tr>
                        <tr>
                            @if($cc2->count())
                                <td id="cl">{{$cc2->first()->info->contact!=null?$cc2->first()->info->contact:'&nbsp;'}} </td>
                            @else
                                <td id="cl">&nbsp;</td>
                            @endif
                        </tr>
                        <tr>
                            @if($cc2->count())
                                <td id="cl">{{$cc2->first()->info->email!=null?$cc2->first()->info->email:"&nbsp;"}} </td>
                            @else
                                <td id="cl">&nbsp;</td>
                            @endif
                        </tr>
                    </table>
                </div>
            </td>
            <td colspan="1" width="2in" valign="top"  id="c">
                <div align="center" ><b>Eligibility Requirements</b></div>

                <div>
                    <table border="0" style="width:100%;">
                        <tr>
                            <td><div>1.)Official transcript of Records (OTR) WITH:
                                </div>
                                <table border="0" style="width:100%;">
                                    <tr>
                                        <td><div>a.) complete scholastic record of athlete</div>
                                    </tr>
                                    <tr>
                                        <td><div>b.) subjects and grades 2nd Sem, SY 2016-2017</div>
                                    </tr>
                                    <tr>
                                        <td><div>c.) subjects enrolled 1st Sem, SY 2017-2018</div>
                                    </tr>
                                    <tr>
                                        <td><div>d.) scanned Picture of Student</div>

                                    </tr>
                                    <tr>
                                        <td><div>e.) School Dry Seal</div>

                                    </tr>

                                </table>
                            </td>

                        </tr>
                        <tr>
                            <td>SCUAA Games Form Numbers 1,2 & 3</td>
                        </tr>
                        <tr>
                            <td>NSO Birth Certificate</td>
                        </tr>

                    </table>
                </div>

            </td>
            <td width="2in" valign="top"  id="c" align="center">
		<table border="0" style="width:100%;">
			<tr>
                		<h3>SCUAA Regional/National Games </h3>
			</tr>
			<tr>
                		<img src="/images/mmsu.png" style="width: 1in;height: 1in">
			</tr>
			<tr>
               			<h4><U>HENEDINE A. AGUINALDO</U> <br>University Registrar</h4>
			</tr>
			<tr>	
                		<h4><U>ROWELL C. TAGATAC</U> <br>Director, CHuMS</h4>
			</tr>
		</table>
            </td>

        </tr>
    </tbody>

</table>












