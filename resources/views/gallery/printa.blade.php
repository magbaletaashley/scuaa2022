<style>
    body {

        margin: 130px 5px 5px 5px;
        text-align:left;
        font-size: 12px;
    }

    table{
        border-collapse:collapse;
    }
    table tbody tr td{
        padding: 2px;;
    }
    ul {
        list-style-type:none;
    }
    li {
        list-style-type:none;
    }
    .page-break {
        page-break-after: always;
    }
    .header{
        position: fixed;
        margin-bottom: 300px;
        padding-bottom: 30px;
    }
    .header {
        top:40px;
    }
    .headert{
        top: 0;
        position: fixed;
    }
    .headert1{
        top: 0;

    }

    .header2{

        margin-bottom: 0px;
        padding-bottom: 0px;
    }
    .header2 {
        top:0px;
    }
    th tr td{
        padding: 0px;
    }
</style>

<div class="header">
    <table  width="100%" style="padding: 0" >
        <tr>
            <td width="50%" align="left">
                <div ><h2>SCUAA NATIONAL GAMES 2017</h2></div>
                <div style="border-bottom: solid 1px;"><h4>February 2017 </h4></div>
                <div>
                    <h4>Region:<div align="right"><div style="border-bottom: solid 1px; width:60%" align="right"></div></div></h4>

                </div>
            </td>
            <td width="15%" align="left"><img src="{{asset('images/scuaa.jpg')}}" style="width: 1.5in; height: 1.5in;" data-holder-rendered="true"></td>

            <td width="35%">
                <div align="right" ><div style="border: solid 1px;width: 30%">SCUAA FORM 2</div></div>
                <div style="border: solid 2px;"><h3>OFFICIAL ENTRY FORM AND GALLERY FOR {{$players->first()->name->first()->desc}}</h3></div>
                <div ><h5>Date of Screening:</h5></div>
                <div>
                    <h4 style="padding-bottom: 0">Region:<div align="right"><div style="border-bottom: solid 1px; width:60%" align="right"></div></div></h4>
                </div>
            </td>
        </tr>
    </table>
</div>
<div>
    @foreach($players->chunk(6) as $player)

        <table width="100%" border="1" id="gal" cellspacing="0" cellpadding="0">

            <tbody>
            <tr>
                <td align="center">
                    <div align="center" >&nbsp;</div>
                    <div align="center" style="border: solid 1px; width: 2in;height: 2in">School Seal Here</div>
                    <div>
                        <table border="1" style="width:100%;">
                            <tr>
                                <td>Name</td>
                            </tr>
                            <tr>
                                <td>Date of Birth</td>
                            </tr>
                            <tr>
                                <td>Course & Year</td>
                            </tr>
                            <tr>
                                <td>School</td>
                            </tr>
                        </table>
                    </div>
                </td>

                @foreach($player as $p)
                    <td align="center">
                        <div align="center" ><b>ATHLETE</b></div>
                        <div align="center" style="border: solid 1px; width: 2in;height: 2in">
                            <img src="{{$p->info->pic}}" style="width: 2in;height: 2in">
                        </div>
                        <div>
                            <table border="1" style="width:100%;">
                                <tr>
                                    <td>{{$p->info->fname}} {{$p->info->mname}} {{$p->info->lname}}</td>
                                </tr>
                                <tr>
                                    <td>{{\Carbon\Carbon::parse($p->info->dob)->format('M-d-Y')}}</td>
                                </tr>
                                <tr>
                                    <td><div>&nbsp;</div></td>
                                </tr>
                                <tr>
                                    <td>Mariano Marcos State University</td>
                                </tr>
                            </table>
                        </div>
                    </td>
                    {{--*/ $j = $loop->index+1 /*--}}
                    <?php $j=$loop->index+1?>
                @endforeach
                @if($j<5)
                    @for($i=$j;$i<=5;$i++)
                        <td align="center">
                            <div align="center" ><b>ATHLETE</b></div>
                            <div align="center" style="border: solid 1px; width: 2in;height: 2in"></div>
                            <div>
                                <table border="1" style="width:100%;">
                                    <tr>
                                        <td><div>&nbsp;</div></td>
                                    </tr>
                                    <tr>
                                        <td><div>&nbsp;</div></td>
                                    </tr>
                                    <tr>
                                        <td><div>&nbsp;</div></td>
                                    </tr>
                                    <tr>
                                        <td><div>&nbsp;</div></td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    @endfor
                @endif


            </tr>

            @if((($loop->index+1)%3)==0 )

                <div class="page-break"></div>

            @endif
            @endforeach
            <tr>
                <td align="center">
                    <div align="center" ><b>&nbsp;</b></div>
                    <div align="center" style="border: solid 1px; width: 2in;height: 2in"></div>
                    <div>
                        <table border="1" style="width:100%;">
                            <tr>
                                <td>Name</td>
                            </tr>
                            <tr>
                                <td>Age</td>
                            </tr>
                            <tr>
                                <td>Contact #/ Mobile no#</td>
                            </tr>
                            <tr>
                                <td>Email</td>
                            </tr>
                        </table>
                    </div>
                </td>

                <td align="center">
                    <div align="center" ><b>COACH</b></div>
                    <div align="center" style="border: solid 1px; width: 2in;height: 2in">
                        @if($hc->count())
                            <img src="{{$hc->first()->info->pic}}" style="width: 2in;height: 2in">
                        @endif
                    </div>
                    <div>
                        <table border="1" style="width:100%;">
                            <tr>
                                @if($hc->count())
                                    <td>{{$hc->first()->info->fname}} {{$hc->first()->info->mname}} {{$hc->first()->info->lname}}</td>
                                @else
                                    <td>&nbsp;</td>
                                @endif
                            </tr>
                            <tr>
                                @if($hc->count())
                                    <td>{{$hc->first()->info->age}} </td>
                                @else
                                    <td>&nbsp;</td>
                                @endif
                            </tr>
                            <tr>
                                @if($hc->count())
                                    <td>{{$hc->first()->info->contact!=null?$hc->first()->info->contact:'&nbsp;'}} </td>
                                @else
                                    <td>&nbsp;</td>
                                @endif
                            </tr>
                            <tr>
                                @if($hc->count())
                                    <td>{{$hc->first()->info->email!=null?$hc->first()->info->email:"&nbsp;"}} </td>
                                @else
                                    <td>&nbsp;</td>
                                @endif
                            </tr>
                        </table>
                    </div>
                </td>
                <td align="center">
                    <div align="center" ><b>ASST. COACH</b></div>
                    <div align="center" style="border: solid 1px; width: 2in;height: 2in">
                        @if($ac->count())
                            <img src="{{$ac->first()->info->pic}}" style="width: 2in;height: 2in">
                        @endif
                    </div>
                    <div>
                        <table border="1" style="width:100%;">
                            <tr>
                                @if($ac->count())
                                    <td>{{$ac->first()->info->fname}} {{$ac->first()->info->mname}} {{$ac->first()->info->lname}}</td>
                                @else
                                    <td>&nbsp;</td>
                                @endif
                            </tr>
                            <tr>
                                @if($ac->count())
                                    <td>{{$ac->first()->info->age}} </td>
                                @else
                                    <td>&nbsp;</td>
                                @endif
                            </tr>
                            <tr>
                                @if($ac->count())
                                    <td>{{$ac->first()->info->contact!=null?$ac->first()->info->contact:'&nbsp;'}} </td>
                                @else
                                    <td>&nbsp;</td>
                                @endif
                            </tr>
                            <tr>
                                @if($ac->count())
                                    <td>{{$ac->first()->info->email!=null?$ac->first()->info->email:"&nbsp;"}} </td>
                                @else
                                    <td>&nbsp;</td>
                                @endif
                            </tr>
                        </table>
                    </div>
                </td>
                <td align="center">
                    <div align="center" ><b>CHAPERON</b></div>
                    <div align="center" style="border: solid 1px; width: 2in;height: 2in">
                        @if($cc->count())
                            <img src="{{$cc->first()->info->pic}}" style="width: 2in;height: 2in">
                        @endif
                    </div>
                    <div>
                        <table border="1" style="width:100%;">
                            <tr>
                                @if($cc->count())
                                    <td>{{$cc->first()->info->fname}} {{$cc->first()->info->mname}} {{$cc->first()->info->lname}}</td>
                                @else
                                    <td>&nbsp;</td>
                                @endif
                            </tr>
                            <tr>
                                @if($cc->count())
                                    <td>{{$cc->first()->info->age}} </td>
                                @else
                                    <td>&nbsp;</td>
                                @endif
                            </tr>
                            <tr>
                                @if($cc->count())
                                    <td>{{$cc->first()->info->contact!=null?$cc->first()->info->contact:'&nbsp;'}} </td>
                                @else
                                    <td>&nbsp;</td>
                                @endif
                            </tr>
                            <tr>
                                @if($cc->count())
                                    <td>{{$cc->first()->info->email!=null?$cc->first()->info->email:"&nbsp;"}} </td>
                                @else
                                    <td>&nbsp;</td>
                                @endif
                            </tr>
                        </table>
                    </div>
                </td>
                <td align="center">
                    <div align="center" ><b></b></div>
                    <div align="center" style="border: solid 1px; width: 2in;height: 2in">
                        PICTURE SIZE 2X2
                    </div>
                    <div>
                        <table border="1" style="width:100%;">
                            <tr>
                                <td>Name</td>
                            </tr>
                            <tr>
                                <td>Date of Birth</td>
                            </tr>
                            <tr>
                                <td>Course & Year</td>
                            </tr>
                            <tr>
                                <td>School</td>
                            </tr>
                        </table>
                    </div>
                </td>
                <td colspan="1" width="2in">
                    <div align="center" ><b>Eligibility Requirements</b></div>

                    <div>
                        <table border="0" style="width:100%;">
                            <tr>
                                <td><div>1.)Official transcript of Records (OTR) WITH:
                                    </div>
                                    <table border="0" style="width:100%;">
                                        <tr>
                                            <td><div>a.) complete scholastic record of athlete</div>
                                        </tr>
                                        <tr>
                                            <td><div>b.) subjects and grades 2nd Sem, SY 2016-2017</div>
                                        </tr>
                                        <tr>
                                            <td><div>c.) subjects enrolled 2nd Sem, SY 2016-2017</div>
                                        </tr>
                                        <tr>
                                            <td><div>d.) scanned Picture of Student</div>

                                        </tr>
                                        <tr>
                                            <td><div>e.) School Dry Seal</div>

                                        </tr>

                                    </table>
                                </td>

                            </tr>
                            <tr>
                                <td>SUCAA Games Form Numbers 1,2 & 3</td>
                            </tr>
                            <tr>
                                <td>NSO Birth Certificate</td>
                            </tr>

                        </table>
                    </div>

                </td>
                <td width="2in">
                    <div align="center"><h3>SCUAA National Games 2017</h3></div>
                    <div  align="center"><h5>University Seal</h5></div>
                    <div style="height: 1in">&nbsp;</div>
                    <div style="border-bottom: solid 1.5px">&nbsp;</div>

                    <div ALIGN="CENTER"> <h4>REGIONAL SPORTS DIRECTOR</h4></div>
                </td>


            </tr>
            </tbody>
        </table>
</div>

