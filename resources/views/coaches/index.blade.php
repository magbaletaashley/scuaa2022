
@extends('layouts.app')

@section('content')
    <div class="container">
        @if(\Illuminate\Support\Facades\Session::has('messages'))
            <div class="row">
                <div class="col-md-2">&nbsp;</div>
                <div class="col-sm-8 alert alert-danger">
                    <center><span >{{ \Illuminate\Support\Facades\Session::get('messages') }}</span></center>
                </div>
            </div>
        @endif

        <div class="row">
            {{ Form::open(array('url'=>route('eve'), 'method'=>'POST')) }}
            {{ csrf_field() }}
            <div class="form-group"></div>
            {{ csrf_field() }}
            {{ Form::close() }}
            <div class="col-md-2">&nbsp;</div>
            {{ Form::open(array('url'=>route('ser'), 'method'=>'POST')) }}
            {{ csrf_field() }}
            <div class="col-md-12" align="right">
                <a href="{{ route('coach.create') }}" class='btn btn-primary'>Add Coach Information</a>
            </div>
            {{csrf_field()}}
            {{Form::close()}}
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-responsive table-condensed">
                    <thead>
                    <td>Name</td>
                    <td>&nbsp;</td>
                    </thead>
                    <script>
                        function ConfirmDelete()
                        {
                            var x = confirm("Are you sure you want to delete?");
                            if (x)
                                return true;
                            else
                                return false;
                        }
                    </script>
                    @foreach($info as $i)
                        <tr>
                            <td class="text-uppercase">{{ $i->lname }}, {{ $i->fname }} {{ $i->mname }}</td>
                            <td width="3">
                                {{ Form::open(array('url'=>route('coach.edit', $i->id), 'method'=>'GET')) }}
                                {{ csrf_field() }}
                                <button type="submit" class="glyphicon glyphicon-edit"></button>
                                {{ csrf_field() }}
                                {{ Form::close() }}
                            </td>
                            <td width="3">
                                {{ Form::open(array('url'=>route('coach.destroy', $i->id), 'method'=>'DELETE', 'onsubmit' => 'return ConfirmDelete()')) }}
                                {{ csrf_field() }}
                                <button type="submit" class="glyphicon glyphicon-trash"></button>
                                {{ csrf_field() }}
                                {{ Form::close() }}
                            </td>
                            
                        </tr>
                    @endforeach

                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">&nbsp;</div>
            <div class="col-md-8" align="center">{{ $info->links() }}</div>
        </div>
    </div>
@endsection