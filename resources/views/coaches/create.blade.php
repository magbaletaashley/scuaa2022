@extends('layouts.app')
@section('content')
    <script>
        function ConfirmCreate()
        {
            var x = confirm("Are you sure of the information you entered?");
            if (x)
                return true;
            else
                return false;
        }
    </script>
    <div class="container">
        @if(count($errors)>=1)
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-8">
                    @foreach($errors->all() as $e)
                        <span class="help-block">{{$e}}</span>
                    @endforeach
                </div>
            </div>
            <br>
        @endif

        {{Form::open(array('url'=>route('coach.store'),'method'=>'POST','files'=>'true', 'onsubmit' => 'return ConfirmCreate()' ))}}
        <div class="row">
            <div class="col-md-8">
                &nbsp;
            </div>
            <div class="col-md-2">&nbsp;</div>
            <div class="col-md-2">
                <div class="form-group">
                    {{Form::label('Upload Image')}}
                    {{ Form::file('image',null,array('class'=>'form-control')) }}
                </div>

            </div>
        </div>
        <br>

        <div class="row">
            <div class="form-group">
                <div align="center"><h1>Coach Information</h1></div>
				</br>
				<div class="col-md-2">{{ Form::label('Name') }}</div>
                <div class="col-md-3">
                    {{ Form::text('lname',null,array('class'=>'form-control','placeholder'=>'Last Name')) }}
                </div>
                <div class="col-md-3">
                    {{ Form::text('fname',null,array('class'=>'form-control','placeholder'=>'First Name')) }}
                </div>
                <div class="col-md-3">
                    {{ Form::text('mname',null,array('class'=>'form-control','placeholder'=>'Middle Name')) }}
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="form-group">
                <div class="col-md-2">{{ Form::label('Birth date') }}</div>
                <div class="col-md-2">
                    {{ Form::date('bday', \Carbon\Carbon::now(),array('class'=>'form-control')) }}
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-2">
                    
                </div>
                <div class="col-md-1">{{ Form::label('Email') }}</div>
                <div class="col-md-2">
                    {{ Form::text('email',null,array('class'=>'form-control','placeholder'=>'E-mail')) }}
                </div>
            </div>
        </div>

        <br>

        <div class="row">
            <div class="form-group">
                <div class="col-md-2">{{ Form::label('Address') }}</div>
                <div class="col-md-4">
                    {{ Form::text('address',null,array('class'=>'form-control','placeholder'=>'Address')) }}
                </div>
                <div class="col-md-1">{{ Form::label('Contact') }}</div>
                <div class="col-md-4">
                    {{ Form::text('contact',null,array('class'=>'form-control','placeholder'=>'Contact Number')) }}
                </div>
				
            </div>
        </div>
        <br>
            <div class="row">
                <div class="form-group">
					<div class="col-md-2">{{ Form::label('Password') }}</div>
                    <div class="col-md-2">
                        <input id="password" type="password" class="form-control" name="password" required>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-2">
                        
                    </div>
                </div>
            </div>
            <br>
       <br>
        <div class="row">
            <div class="col-md-10">&nbsp;</div>
            <div class="col-md-2">
                {{Form::submit('Save Information',array('class'=>'btn btn-success'))}}
            </div>
        </div>

        {{ Form::close() }}
    </div>

@endsection