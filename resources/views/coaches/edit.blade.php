<?php
    use App\User;
?>

@extends('layouts.app')

@section('content')
    <script>
        function ConfirmCreate()
        {
            var x = confirm("Are you sure of the information you entered?");
            if (x)
                return true;
            else
                return false;
        }
    </script>
    <div class="container">
        @if(count($errors)>=1)
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-8">

                    @foreach($errors->all() as $c)
                        <span class="help-block">{{ $c }}</span>
                    @endforeach

                </div>
            </div>
            <br>
        @endif

        {{ Form::model($info, array('url'=>route('coach.update', $info->id), 'method'=>'PATCH', 'files'=>'true', 'onsubmit' => 'return ConfirmCreate()' )) }}
        <div class="row">
            <div class="col-md-8">&nbsp;</div>
            <div class="col-md-2">&nbsp;</div>
            <div class="col-md-2">
                <div class="form-group">
                    <img src="{{ $info->pic }}" class="img-responsive img-circle">
                    {{ Form::label('Upload Image') }}
                    {{ Form::file('image', null, array('class'=>'form-control')) }}
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="form-group">
				<div align="center"><h1>Coach Information</h1></div>
				</br>
                <div class="col-md-2">{{ Form::label('Name') }}</div>
                <div class="col-md-3">
                    {{ Form::text('lname', null, array('class'=>'form-control', 'placeholder'=>'Last Name')) }}
                </div>
                <div class="col-md-3">
                    {{ Form::text('fname', null, array('class'=>'form-control','placeholder'=>'First Name')) }}
                </div>
                <div class="col-md-3">
                    {{ Form::text('mname', null, array('class'=>'form-control','placeholder'=>'Middle Name')) }}
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="form-group">
                <div class="col-md-2">{{ Form::label('Birth date') }}</div>
                <div class="col-md-2">
                    {{ Form::date('bday', null,array('class'=>'form-control')) }}
                </div>
                <div class="col-md-1">{{ Form::label('Age') }}</div>
                <div class="col-md-2">
                    {{ Form::text('age', null, array('class'=>'form-control','placeholder'=>'Age', 'disabled')) }}
                </div>
                <div class="col-md-1">{{ Form::label('Email') }}</div>
                <div class="col-md-2">
                    {{ Form::text('email', null, array('class'=>'form-control', 'placeholder'=>'E-mail')) }}
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="form-group">
                <div class="col-md-2">{{ Form::label('Address') }}</div>
                <div class="col-md-4">
                    {{ Form::text('address', null, array('class'=>'form-control', 'placeholder'=>'Address')) }}
                </div>
                <div class="col-md-1">{{ Form::label('Contact #') }}</div>
                <div class="col-md-4">
                    {{ Form::text('contact', null, array('class'=>'form-control', 'placeholder'=>'Contact Number')) }}
                </div>
            </div>
        </div>
        @if( ( (User::where('id', Auth::id())->get()->first()->faculty_id) == $info->email) || (User::where('id', Auth::id())->get()->first()->userType) == 'admin')
        <br>
            <div class="row">
                <div class="form-group">
                    <div class="col-md-2">{{ Form::label('Password') }}</div>
                    <div class="col-md-2">
                        <input id="password" type="password" class="form-control" name="password" value="{{ $info->password }}" required>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-2">
                        
                    </div>
                </div>
            </div>
        @else
            {{ Form::hidden('password', $info->password) }}
        @endif
        <br>
        <br>
        <div class="row">
            <div class="col-md-10">&nbsp;</div>
            <div class="col-md-2">
                {{ Form::submit('Save Information', array('class'=>'btn btn-success')) }}
            </div>
        </div>
        <script>
            function ConfirmDelete()
            {
                var x = confirm("Are you sure you want to delete?");
                if (x)
                    return true;
                else
                    return false;
            }
        </script>
        {{ Form::close() }}
        {{ Form::open(array('url'=>route('coach.destroy', $info->id), 'method'=>'DELETE', 'onsubmit' => 'return ConfirmDelete()')) }}
            
        {{ Form::close() }}
        <div class="row">
            <div class="col-md-12">
			<hr/>
			<div align="center"><h1>Event Information</h1></div>
				</br>
                <table class="table table-striped table-responsive table-condensed">
                    <thead>
                        <tr>
                            <td>&nbsp;</td>
                            <td>Event</td>
                            <td>Status</td>
                            <td>&nbsp;</td>
                        </tr>
                    </thead>

                    @if($info->events->count())
                        @foreach($info->events as $e)
                            <tr>
                                <td>&nbsp;</td>
                                <td>{{ $e->name->desc }}</td>
                                <td>
                                    {{ Form::select('status', array(1=>'Head Coach', 2=>'Asst. Coach', 3=>'Coach 1', 4=>'Coach 2'), $e->status, array('class'=>'form-control', 'disabled')) }}
                                </td>
								<td>&nbsp;</td>
                                <td width="3">
                                    {{ Form::open(array('url'=>route('evet2', $e->name->id), 'method'=>'GET')) }}
                                        {{ Form::submit('Show', array('class'=>'btn btn-success')) }}
                                    {{ Form::close() }}
                                </td>
                                <td width="3">
                                    {{ Form::open(array('url'=>route('ce.destroy', $e->id), 'method'=>'DELETE')) }}
                                        {{ Form::submit('Delete', array('class'=>'btn btn-danger')) }}
                                    {{ Form::close() }}
                                </td>
                            </tr>
                        @endforeach
                    @endif

                        <tr>
                            <td>&nbsp;</td>
                            {{ Form::open(array('url'=>route('ce.store'), 'method'=>'POST', 'onsubmit' => 'return ConfirmCreate()')) }}
                            <td>
                                {{ Form::hidden('coach_id', $info->id) }}
                                {{ Form::select('event', $events->pluck('desc', 'id'), null, array('class' => 'form-control')) }}
                            </td>
                            <td>
                                {{ Form::select('status', array(1=>'Head Coach', 2=>'Asst. Coach', 3=>'Coach 1', 4=>'Coach 2'), null, array('class'=>'form-control')) }}
                            </td>
							<td width="3">&nbsp;</td>
							<td width="3">&nbsp;</td>
                            <td width="3">
                                {{ Form::submit('Add Event', array('class'=>'btn btn-success')) }}
                            </td>
                            {{ Form::close() }}
                      </tr>
                </table>
            </div>
        </div>
    </div>
@endsection