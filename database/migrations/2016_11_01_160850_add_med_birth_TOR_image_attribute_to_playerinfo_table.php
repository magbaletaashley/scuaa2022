<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMedBirthTORImageAttributeToPlayerinfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('playerinfo', function (Blueprint $table) {
            $table->string('medicalCertificate')->nullable();
            $table->string('birthCertificate')->nullable();
            $table->string('TOR')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('playerinfo', function (Blueprint $table) {
            $table->dropColumn('medicalCertificate');
            $table->dropColumn('birthCertificate');
            $table->dropColumn('TOR');
        });
    }
}
