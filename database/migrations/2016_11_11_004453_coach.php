<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Coach extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('coach',function(Blueprint $table){
            $table->increments('id');

            $table->string('lname');
            $table->string('fname');
            $table->string('mname')->nullable();
            $table->string('contact');
            $table->string('pic');
            $table->string('email');
            $table->string('bday');
            $table->integer('age');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('coach');
    }
}
