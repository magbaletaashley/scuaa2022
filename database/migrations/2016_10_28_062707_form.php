<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Form extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('playerInfo',function(Blueprint $table){
            $table->increments('id');
            $table->string('student_number')->unique();
            $table->string('lname');
            $table->string('fname');
            $table->string('mname')->nullable();
            $table->string('dob');
            $table->integer('age');
            $table->string('weight');
            $table->string('height');
            $table->string('address');
            $table->string('blood_type');
            $table->string('allergies')->nullable();
            $table->string('medication')->nullable();
            $table->string('contact')->nullable();
            $table->string('pic');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('playerInfo');
    }
}
