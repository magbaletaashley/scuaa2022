<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddScuaaCategoryAttributeToPlayerinfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('playerInfo', function (Blueprint $table) {
            $table->integer('scuaaCategory')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('playerInfo', function (Blueprint $table) {
            $table->dropColumn('scuaaCategory');
        });
    }
}
