<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CoachEvents extends Model
{
    //
    protected $table='coachevents';
    protected $fillable=['event','status','coach_id'];
    public function name(){
        return $this->hasOne('App\Events','id','event');
    }
    public function info(){
        return $this->belongsTo('App\Coach','coach_id','id');
    }
}
