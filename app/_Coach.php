<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coach extends Model
{
    //
    protected $table='coach';
    protected $fillable=['lname','fname','mname','contact','email','bday','age','address'];
    public function events(){
        return $this->hasMany('App\CoachEvents','coach_id','id');
    }
}
