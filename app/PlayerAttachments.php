<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlayerAttachments extends Model
{
    protected $table='playerattachments';

    protected $fillable=['student_number','attachment_name', 'attachment'];
    
    public function docuName(){
        return $this->hasMany('App\PlayerAttachments', 'student_number', 'student_number');
    }
    public function attach(){
        return $this->belongsTo('App\PlayerAttachments', 'student_number', 'student_number');
    }
    public function info(){
        return $this->belongsTo('App\PlayerInfo', 'student_number', 'student_number');
    }
}
