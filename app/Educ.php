<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Educ extends Model
{
    //
    protected $table='educ';
    protected $fillable=[
        'student_number','level','year_grad'
    ];
}
