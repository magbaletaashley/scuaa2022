<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AgeProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::directive('age', function($expression){
            $data = json_decode($expression);

            $year = $data[0];
            $month = $data[1];
            $day = $data[2];
            
            $age = Carbon::createFromDate($year, $month, $day)->age;

            return "<?php echo $age; ?>";
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
