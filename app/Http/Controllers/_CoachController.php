<?php

namespace App\Http\Controllers;

use App\Coach;
use App\Events;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Validator;

class CoachController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    } 
    public function index()
    {
        //
        $info=Coach::paginate(20);
        return view('coaches.index',compact('info'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        return view('coaches.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $messages=array(

            'lname.required'=>'Last Name is Required',
            'fname.required'=>'First Name is Required',
            'age.required'=>'Age is Required',


        );
        $rules=array(
            'image'=>'required|image',

            'lname'=>'required',
            'fname'=>'required',
            'bday'=>'required',
            'age'=>'required',

            'address'=>'required',
            'email'=>'required'


        );
        $validator=Validator::make($request->all(),$rules,$messages)->validate();
        $coach=new Coach();

        if($request->hasFile('image')){
            $d=Carbon::now();
            $name=$request->get('lname').''.$d->day.str_random(4).$d->hour.$d->second;
            $p1='\images\\';
            $path= base_path() . '/public/images/';
            $request->file('image')->move($path,$name.'.'.$request->file('image')->getClientOriginalExtension());
            $coach->pic=$p1.$name.'.'.$request->file('image')->getClientOriginalExtension();
        }
        $coach->fill($request->all());
        $coach->save();
        return redirect()->route('coach.edit',$coach->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

        $events=Events::get();
        $info=Coach::where('id',$id)->get();
        if($info->count()) {
            $info=$info->first();
            $e = array();
            if ($info->events->count()) {
                foreach ($info->events as $pe) {
                    $e[$pe->name->first()->desc] = $pe->name->first()->desc;
                }
            }
            return view('coaches.edit', compact('info', 'events', 'e'));
        } else
            return redirect()->route('form.index')->with('messages','No Record Found');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $c=Coach::where('id',$id)->get()->first();
        $c->delete();
        $c->events()->delete();
        return redirect()->back();
    }
}
