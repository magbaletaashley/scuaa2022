<?php

namespace App\Http\Controllers;

use App\CoachEvents;
use App\Events;
use App\PlayerInfo;
use Illuminate\Http\Request;
use App\PlayerEvents;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use App\Educ;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;

class MasterListController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }     
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $test = User::where('id', Auth::id())->where('userType', 'admin')->count();
        $test2 = User::where('id', Auth::id())->where('userType', 'coach')->count();

        if($test == 0 && $test2 == 0)
        {
            $shit = User::where('id', Auth::id())->get();
            foreach ($shit as $a) {
                $pass = $a->faculty_id;
            }

            return redirect()->route('form.edit', Crypt::encrypt($pass));
        }
        else
        {
            $events=Events::get();

            return view('masterlist.index',compact('events'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $test = User::where('id', Auth::id())->where('userType', 'admin')->count();
        $test2 = User::where('id', Auth::id())->where('userType', 'coach')->count();

        if($test == 0 && $test2 == 0)
        {
            $shit = User::where('id', Auth::id())->get();
            foreach ($shit as $a) {
                $pass = $a->faculty_id;
            }

            return redirect()->route('form.edit', Crypt::encrypt($pass));
        }
        else
        {
            PlayerEvents::where('status', 0)->update(array('status' => 1));

            $e = Events::get();

            $players =  PlayerInfo::get();

            $hc=CoachEvents::get();

            $course = Educ::get();

            //  return $players->count();
            return view('masterlist.print', compact('players','e','hc', 'course'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
