<?php

namespace App\Http\Controllers;

use App\Educ;
use App\Events;
use App\PlayerEvents;
use App\PlayerInfo;
use App\PlayerAttachments;
use Carbon\Carbon;
use Dompdf\Adapter\CPDF;
use Dompdf\Adapter\PDFLib;
use Dompdf\Dompdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use PDF;
use App\Http\Requests;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Coach;

use Illuminate\Support\Facades\Validator;

use Illuminate\Support\Facades\Crypt;

use Illuminate\Support\Facades\Input;

class FormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth')->except('create', 'store', 'edit', 'update', 'show', 'destroy');
    } 

    public function index()
    {
        //
        //PlayerInfo::get()->update(array('lname'=>ucwords()))
        $test = User::where('id', Auth::id())->where('userType', 'admin')->count();
        $test2 = User::where('id', Auth::id())->where('userType', 'coach')->count();
        $events=Events::get();

        if($test == 0 && $test2 == 0)
        {
            $shit = User::where('id', Auth::id())->get();
            foreach ($shit as $a) {
                $pass = $a->faculty_id;
            }

            return redirect()->route('form.edit', Crypt::encrypt($pass));
        }
        else if($test2 != 0)
        {
            $shit = User::where('id', Auth::id())->get()->first()->faculty_id;
            $shit = Coach::where('email', $shit)->get()->first()->id;

            return redirect()->route('coach.edit', $shit);
        }
        else
        {
            //$info=PlayerInfo::orderby('lname','ASC')->where('deleted_at', null)->paginate(20);
			
			$info=PlayerInfo::orderby('lname','ASC')->whereHas('events', function ($query){
                $query->whereIn('status', [1]);})->paginate(20);
			
            //$info = PlayerInfo::where('lname', 'soh')->paginate(20);
            return view('home', compact('info', 'events'));
        }
    }

    public function showResults($id)
    {
        $info = PlayerInfo::orderby('lname', 'ASC')->where('lname', $id)->paginate(20);
        $events=Events::get();
        $test = User::where('id', Auth::id())->where('userType', 'admin')->count();
        $test2 = User::where('id', Auth::id())->where('userType', 'coach')->count();
        
        if($test == 0 && $test2 == 0) 
        {
            $shit = User::where('id', Auth::id())->get();
            foreach ($shit as $a) {
                $pass = $a->faculty_id;
            }

            return redirect()->route('form.edit', Crypt::encrypt($pass));
        }
        else
        {
            if($info->count())
            return view('home',compact('info', 'events'));
            else
            {
                $info = PlayerInfo::orderby('lname', 'ASC')->where('fname', $id)->paginate(20);
                return view('home',compact('info', 'events'));
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $events=Events::get();

        return view('create', compact('events'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $player=new PlayerInfo();
        $educ=new Educ();
        $pEvents= new PlayerEvents();
        //$pa = new PlayerAttachments();
        $rules=array(
            'image'=>'image|required',
            'student_number'=>'required|unique:playerinfo|alpha_dash|between:7,10',
            'lname'=>'required',
            'fname'=>'required',
            'dob'=>'required',
            'weight'=>'required',
            'address'=>'required',
            //'blood_type'=>'required',
            'height'=>'required',
            'elementary.name'=>'required',
            'elementary.year'=>'required|date_format:Y|digits:4',
            'high.name'=>'required',
            'high.year'=>'required|date_format:Y|digits:4',
            'col.year' => 'date_format:Y|digits:4',
            'trans.year' => 'date_format:Y|digits:4',
            'password'=>'required'
        );
        $messages=array(
            'student_number.required'=>'Student Number is Required',
            'image.image'=>'File uploaded must be an image',
            'student_number.alpha_dash'=>'Student Number must be of the format (11-11111)',
            'student_number.between:7,10'=>'Student Number must be of the format(11-11111)',
            'lname.required'=>'Last Name is Required',
            'fname.required'=>'First Name is Required',
            'elementary.name'=>'Fill up your Elementary Education Background is required',
            'elementary.year'=>'Fill up your Elementary Education Background is required',
            'high.name'=>'Fill up your High School Education Background is required',
            'high.year'=>'Fill up your High SchoolEducation Background is required'
        );
        $validator = Validator::make($request->all(), $rules, $messages)->validate();
        //return $request->all();

        $player->fill($request->all());
        if($request->hasFile('image')){
            $d=Carbon::now();
            $name=$request->get('student_number').'_'.$request->get('lname').''.$d->day.str_random(4).$d->hour.$d->second;
            $p1='\images\\';
           $path= base_path() . '/public/images/';
            $request->file('image')->move($path,$name.'.'.$request->file('image')->getClientOriginalExtension());
            $player->pic=$p1.$name.'.'.$request->file('image')->getClientOriginalExtension();
        }
        if($request->hasFile('medCert')){
            $d=Carbon::now();
            $name=$request->get('student_number').'_'.$request->get('lname').''.$d->day.str_random(4).$d->hour.$d->second;
            $p1='\images\\';
           $path= base_path() . '/public/images/';
            $request->file('medCert')->move($path,$name.'.'.$request->file('medCert')->getClientOriginalExtension());
            $player->medicalCertificate=$p1.$name.'.'.$request->file('medCert')->getClientOriginalExtension();
        } 
        if($request->hasFile('birthCert')){
            $d=Carbon::now();
            $name=$request->get('student_number').'_'.$request->get('lname').''.$d->day.str_random(4).$d->hour.$d->second;
            $p1='\images\\';
           $path= base_path() . '/public/images/';
            $request->file('birthCert')->move($path,$name.'.'.$request->file('birthCert')->getClientOriginalExtension());
            $player->birthCertificate=$p1.$name.'.'.$request->file('birthCert')->getClientOriginalExtension();
        } 
        if($request->hasFile('TOR')){
            $d=Carbon::now();
            $name=$request->get('student_number').'_'.$request->get('lname').''.$d->day.str_random(4).$d->hour.$d->second;
            $p1='\images\\';
           $path= base_path() . '/public/images/';
            $request->file('TOR')->move($path,$name.'.'.$request->file('TOR')->getClientOriginalExtension());
            $player->TOR=$p1.$name.'.'.$request->file('TOR')->getClientOriginalExtension();
        } 
        $shit = Carbon::createFromFormat('Y-m-d', $player->dob);
        $player->age = $player->age($shit);
        $player->save();
        //$pEvents->fill($request->all());
       // $pEvents->events_id=$request->get('event');
        //$pEvents->save();
        $elem=$request->get('elementary');
        $high=$request->get('high');
        $col=$request->get('col');
        $trans=$request->get('trans');
        //add educ
        $educ=new Educ();
        $educ->student_number=$request->get('student_number');
        $educ->level='Elem';
        $educ->name=$elem['name'];
        $educ->year_grad=$elem['year'];
        $educ->save();
        //high
        $educ=new Educ();
        $educ->student_number=$request->get('student_number');
        $educ->level='High';
        $educ->name=$high['name'];
        $educ->year_grad=$high['year'];
        $educ->save();
        //col
        if($col['name']!=""){
            $educ=new Educ();
            $educ->student_number=$request->get('student_number');
            $educ->level='Col';
            $educ->name=$col['name'];
            $educ->year_grad=$col['year'];
            $educ->save();
        }
        //trans
        if($trans['name']!=null){
            $educ=new Educ();
            $educ->student_number=$request->get('student_number');
            $educ->level='Trans';
            $educ->name=$trans['name'];
            $educ->year_grad=$trans['year'];
            $educ->save();
        }

        $player->deleted_at = $player->created_at;
        $userAthlete = new User();
        $userAthlete->faculty_id = $player->student_number;
        $userAthlete->fullName = $player->fname;
        $userAthlete->password = bcrypt($player->password);
        $userAthlete->userType = "athlete";
        $userAthlete->save();
        $player->save();

        $events=Events::get();

        $info=PlayerInfo::orderby('lname','ASC')->where('deleted_at', null)->paginate(20);

        //$info = PlayerInfo::where('lname', 'soh')->paginate(20);

        //if(Auth::check()
        $parameter = $request->get('student_number');
        $parameter = Crypt::encrypt($parameter);

        $athletes = PlayerInfo::get();

        foreach($athletes as $athlete)
        {
            if((User::where('faculty_id', $athlete->student_number))->count() >= 1)
            {
                
            }
            else
            {
                $userAthlete = new User();
                $userAthlete->faculty_id = $athlete->student_number;
                $userAthlete->fullName = $athlete->fname;
                $userAthlete->password = bcrypt("admins");
                $userAthlete->userType = "athlete";
                $userAthlete->save();
            }
        }

        return redirect()->route('form.edit', $parameter);
        //else
            //return view('auth.login');

        

    }
    public function search(Request $request){
        //return 'hello';
        //return redirect()->route('showResults');

        $test = User::where('id', Auth::id())->where('userType', 'admin')->count();
        $test2 = User::where('id', Auth::id())->where('userType', 'coach')->count();
        
        if($test == 0 && $test2 == 0)
        {
            $shit = User::where('id', Auth::id())->get();
            foreach ($shit as $a) {
                $pass = $a->faculty_id;
            }

            return redirect()->route('form.edit', Crypt::encrypt($pass));
        }
        else
        {
            $info=PlayerInfo::where('student_number',$request->get('student_number'))->get();

            $infolName=PlayerInfo::where('lname',$request->get('student_number'))->get();

            $infofName=PlayerInfo::where('fname',$request->get('student_number'))->get();

            $parameter = $request->get('student_number');
            $parameter = Crypt::encrypt($parameter);

            if($info->count())
            {
                return redirect()->route('form.edit', $parameter);
            }
            else if($infolName->count() || $infofName->count())
            {
                return redirect()->route('showResults', $request->get('student_number'));
            }
            else
                return redirect()->route('form.index')->with('messages','No student');
            }
    }
    public function eventSearch(Request $request){
        //  return $request->all();//get('events_id');
        //  return $request->get('events_id');
		
        $test = User::where('id', Auth::id())->where('userType', 'admin')->count();
        $test2 = User::where('id', Auth::id())->where('userType', 'coach')->count();

        if($test == 0 && $test2 == 0)
        {
            $shit = User::where('id', Auth::id())->get();
            foreach ($shit as $a) {
                $pass = $a->faculty_id;
            }

            return redirect()->route('form.edit', Crypt::encrypt($pass));
        }
        else
            return redirect()->route('evet', $request->get('events_id'));
    }
    public function evet($id){
        $test = User::where('id', Auth::id())->where('userType', 'admin')->count();
        $test2 = User::where('id', Auth::id())->where('userType', 'coach')->count();

        if($test == 0 && $test2 == 0)
        {
            $shit = User::where('id', Auth::id())->get();
            foreach ($shit as $a) {
                $pass = $a->faculty_id;
            }

            return redirect()->route('form.edit', Crypt::encrypt($pass));
        }
        else
        {
            $events = Events::get();
			
            $info = PlayerEvents::where('events_id', $id)->paginate(20);
				
            return view('evet', compact('info', 'events','id'));
        }
    }
    public function evet2($id){
        $test = User::where('id', Auth::id())->where('userType', 'admin')->count();
        $test2 = User::where('id', Auth::id())->where('userType', 'coach')->count();

        if($test == 0 && $test2 == 0)
        {
            $shit = User::where('id', Auth::id())->get();
            foreach ($shit as $a) {
                $pass = $a->faculty_id;
            }

            return redirect()->route('form.edit', Crypt::encrypt($pass));
        }
        else
        {
            $events = Events::get();
            $info = PlayerEvents::where('events_id', $id)->paginate(20);
            //return decrypt($id);
            return view('evet2', compact('info', 'events'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $events=Events::get();
        $info=PlayerInfo::where('student_number',$id)->get()->first();
        $pdf = PDF::loadView('print.test');

        return $pdf->download('invoice.pdf');
        //return view('print.form',compact('info','events'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = Crypt::decrypt($id);
        $events = Events::get();
        $info = PlayerInfo::withTrashed()->where('student_number', $id)->get();

            if($info->count())
            {
                $info = $info->first();
                $e = array();
                $a = array();

                if($info->events->count()) 
                {
                    foreach ($info->events as $pe) 
                    {
                        $e[$pe->name->first()->desc] = $pe->name->first()->desc;
                    }
                }

                if($info->documents->count())
                {
                    foreach($info->documents as $pa)
                    {
                        $a[$pa->docuName->first()->document] = $pa->docuName->first()->document;
                    }
                }
                $shit = Carbon::createFromFormat('Y-m-d', $info->dob);
                $info->age = $info->age($shit);
                $info->save();

                return view('edit', compact('info', 'events', 'e', 'a'));
            } else
                return redirect()->route('form.index')->with('messages','No Record Found');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $id = Crypt::decrypt($id);
        $player = PlayerInfo::withTrashed()->where('student_number', $id)->get()->first();
        $elem = $player->elem->first();
        $high = $player->high->first();
        $col = $player->col;
        $trans = $player->trans;
        $pEvents = $player->events->first();
        //$pAttachments = $player->documents->first();

        $messages=array(
            'student_number.required'=>'Student Number is Required',
            'lname.required'=>'Last Name is Required',
            'fname.required'=>'First Name is Required',
            'elementary.name'=>'Fill up your Elementary Education Background is required',
            'elementary.year'=>'Fill up your Elementary Education Background is required',
            'high.name'=>'Fill up your High School Education Background is required',
            'high.year'=>'Fill up your High SchoolEducation Background is required',
        );
        $rules=array(
            //'image'=>'required|image',
            //'student_number'=>'required',
            'lname'=>'required',
            'fname'=>'required',
            'dob'=>'required',
            'weight'=>'required',
            'address'=>'required',
            //'blood_type'=>'required',
            'height'=>'required',
            'elementary.name'=>'required',
            'elementary.year'=>'required',
            'high.name'=>'required',
            'high.year'=>'required',
            'password'=>'required'
        );
        $validator = Validator::make($request->all(), $rules, $messages)->validate();

        $player->fill($request->all());
        if($request->hasFile('image'))
        {
            $d = Carbon::now();
            $name = $request->get('student_number').'_'.$request->get('lname').''.$d->day.str_random(4).$d->hour.$d->second;
            $p1 = '\images\\';
            $path = base_path() . '/public/images/';
            $request->file('image')->move($path,$name.'.'.$request->file('image')->getClientOriginalExtension());
            $player->pic=$p1.$name.'.'.$request->file('image')->getClientOriginalExtension();
        }
        if($request->hasFile('medCert')){
            $d=Carbon::now();
            $name=$request->get('student_number').'_'.$request->get('lname').''.$d->day.str_random(4).$d->hour.$d->second;
            $p1='\images\\';
           $path= base_path() . '/public/images/';
            $request->file('medCert')->move($path,$name.'.'.$request->file('medCert')->getClientOriginalExtension());
            $player->medicalCertificate=$p1.$name.'.'.$request->file('medCert')->getClientOriginalExtension();
        } 
        if($request->hasFile('birthCert')){
            $d=Carbon::now();
            $name=$request->get('student_number').'_'.$request->get('lname').''.$d->day.str_random(4).$d->hour.$d->second;
            $p1='\images\\';
           $path= base_path() . '/public/images/';
            $request->file('birthCert')->move($path,$name.'.'.$request->file('birthCert')->getClientOriginalExtension());
            $player->birthCertificate=$p1.$name.'.'.$request->file('birthCert')->getClientOriginalExtension();
        } 
        if($request->hasFile('TOR')){
            $d=Carbon::now();
            $name=$request->get('student_number').'_'.$request->get('lname').''.$d->day.str_random(4).$d->hour.$d->second;
            $p1='\images\\';
           $path= base_path() . '/public/images/';
            $request->file('TOR')->move($path,$name.'.'.$request->file('TOR')->getClientOriginalExtension());
            $player->TOR=$p1.$name.'.'.$request->file('TOR')->getClientOriginalExtension();
        } 
        $player->save();
       // $pEvents->fill($request->all());
       // $pEvents->events_id=$request->get('event');
        //$pEvents->save();
        //elem
        $e=$request->get('elementary');
        $elem->name=$e['name'];
        $elem->year_grad=$e['year'];
        $elem->save();
        //high
        $h=$request->get('high');
        $high->name=$h['name'];
        $high->year_grad=$h['year'];
        $high->save();
        //col
        $c=$request->get('col');

        if($col->count()<=0 && $c['name']!=''){

            $educ=new Educ();
            $educ->student_number=$request->get('student_number');
            $educ->level='Col';
          $educ->name=$c['name'];
            $educ->year_grad=$c['year'];
          //  return $educ;
            $educ->save();
        }else if($col->count()>=1 && $c['name']!=''){
            $col=$player->col->first();
            $col->name=$c['name'];
            $col->year_grad=$c['year'];
            $col->save();
        }
        //trans
        $t=$request->get('trans');
        //return $trans;
        if($trans->count()<=0 && $t['name']!=''){
            $educ=new Educ();
            $educ->student_number=$request->get('student_number');
            $educ->level='Trans';
            $educ->name=$t['name'];
            $educ->year_grad=$t['year'];
            $educ->save();
        }else if($trans->count()>=1 && $t['name']!=''){
            $trans=$player->trans->first();
            $trans->name=$t['name'];
            $trans->year_grad=$t['year'];
            $trans->save();
        }

        $editedAdmin = User::where('faculty_id', $player->student_number)->get()->first();
        $editedAdmin->password = bcrypt($player->password);
        $editedAdmin->save();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $info=PlayerInfo::withTrashed()->where('student_number', $id)->get()->first();

        $deleteAdminInfo = User::where('faculty_id', $id)->get()->first();

        $deleteAdminInfo->forceDelete();

        foreach($info->educ as $e){
            $e->forceDelete();
        }
        foreach($info->events as $e){
            $e->forceDelete();
        }
        foreach($info->documents as $e){
            $e->forceDelete();
        }
        
        $info->forceDelete();
        return redirect()->back()->with('messages','Delete Record');
    }

    public function addAdmin()
    {
        $test = User::where('id', Auth::id())->where('userType', 'admin')->count();

        if($test == 0)
        {
            return redirect()->back();
        }
        else
            return view('addAdmin');
    }

    public function register(Request $request)
    {
        $test = User::where('id', Auth::id())->where('userType', 'admin')->count();

        if($test == 0)
        {
            return redirect()->back();
        }
        else
        {
            $this->validator($request->all())->validate();

            $user = $this->createAdmin($request->all());

            $this->guard()->login($user);

            $events=Events::get();

            $info=PlayerInfo::orderby('lname','ASC')->paginate(20);

            return view('home', compact('info', 'events'));
        }
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'faculty_id' => 'required|max:255|unique:users',
            'fullName' => 'required|max:255',
            'password' => 'required|min:6|confirmed'
        ]);
    }

    protected function guard()
    {
        return Auth::guard();
    }

    protected function createAdmin(array $data)
    {
        return User::create([
            'faculty_id' => $data['faculty_id'],
            'fullName' => $data['fullName'],
            'password' => bcrypt($data['password'])
        ]);
    }

    public function adminList()
    {
        $test = User::where('id', Auth::id())->where('userType', 'admin')->count();

        if($test == 0)
        {
            return redirect()->back();
        }
        else
        {
            $admins = User::all();

            return view('adminList', compact('admins'));
        }
    }

    public function destroyAdmin($id)
    {
        $test = User::where('id', Auth::id())->where('userType', 'admin')->count();

        if($test == 0)
        {
            return redirect()->back();
        }
        else
        {
            $admin = User::findOrFail($id);
            $admin->forceDelete();

            return redirect('/login');
        }
    }

    public function unconfirmed()
    {
        $test = User::where('id', Auth::id())->where('userType', 'admin')->count();
        $test2 = User::where('id', Auth::id())->where('userType', 'coach')->count();

        if($test == 0 && $test2 == 0)
        {
            $shit = User::where('id', Auth::id())->get();
            foreach ($shit as $a) {
                $pass = $a->faculty_id;
            }

            return redirect()->route('form.edit', Crypt::encrypt($pass));
        }
        else
        {
            $events=Events::get();

            $info = PlayerInfo::orderby('lname', 'ASC')->onlyTrashed()->paginate(20);

            return view('home2', compact('info', 'events'));
        }
    }

    public function confirm($id)
    {
        $test = User::where('id', Auth::id())->where('userType', 'admin')->count();
        $test2 = User::where('id', Auth::id())->where('userType', 'coach')->count();

        if($test == 0 && $test2 == 0)
        {
            $shit = User::where('id', Auth::id())->get();
            foreach ($shit as $a) {
                $pass = $a->faculty_id;
            }

            return redirect()->route('form.edit', Crypt::encrypt($pass));
        }
        else
        {
            $info = PlayerInfo::withTrashed()->where('student_number', $id)->get()->first();

            $info->deleted_at = null;
            $info->save();

            $events=Events::get();

            $info = PlayerInfo::orderby('lname', 'ASC')->paginate(20);

            return view('home', compact('info', 'events'));
        }
    }
}

