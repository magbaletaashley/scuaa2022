<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\PlayerAttachments;

use App\Http\Requests;

use Carbon\Carbon;

use Response;

class AttachmentsController extends Controller
{ 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pa = new PlayerAttachments();

        $pa->fill($request->all());

        if($request->hasFile('image')){
            $d = Carbon::now();
            $name = $request->get('student_number').''.$d->day.str_random(4).$d->hour.$d->second;
            $p1 ='\attachments\\';
            $path = base_path() . '/public/attachments/';
            $request->file('image')->move($path, $name.'.'.$request->file('image')->getClientOriginalExtension());
            $pa->attachment = $p1.$name.'.'.$request->file('image')->getClientOriginalExtension();
        }

        $pa->save();

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $toDownload = PlayerAttachments::where('id', $id)->get()->first();

        return Response::download(public_path($toDownload->attachment)); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pl=PlayerAttachments::where('id', $id)->get()->first();
        $pl->delete();
        return redirect()->back();
    }
}
