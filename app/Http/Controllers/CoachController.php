<?php

namespace App\Http\Controllers;

use App\Coach;
use App\Events;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

use App\Http\Requests;
use Illuminate\Support\Facades\Validator;

class CoachController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    } 

    public function index()
    {
        $test = User::where('id', Auth::id())->where('userType', 'admin')->count();
        $test2 = User::where('id', Auth::id())->where('userType', 'coach')->count();

        if($test == 0 && $test2 == 0)
        {
            $shit = User::where('id', Auth::id())->get();
            foreach ($shit as $a) {
                $pass = $a->faculty_id;
            }

            return redirect()->route('form.edit', Crypt::encrypt($pass));
        }
        else
        {
            $info = Coach::orderby('lname', 'ASC')->paginate(20);

            return view('coaches.index', compact('info'));
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $test = User::where('id', Auth::id())->where('userType', 'admin')->count();
        $test2 = User::where('id', Auth::id())->where('userType', 'coach')->count();

        if($test == 0 && $test2 == 0)
        {
            $shit = User::where('id', Auth::id())->get();
            foreach ($shit as $a) {
                $pass = $a->faculty_id;
            }

            return redirect()->route('form.edit', Crypt::encrypt($pass));
        }
        else
            return view('coaches.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $test = User::where('id', Auth::id())->where('userType', 'admin')->count();
        $test2 = User::where('id', Auth::id())->where('userType', 'coach')->count();

        if($test == 0 && $test2 == 0)
        {
            $shit = User::where('id', Auth::id())->get();
            foreach ($shit as $a) {
                $pass = $a->faculty_id;
            }

            return redirect()->route('form.edit', Crypt::encrypt($pass));
        }
        else
        {
            $messages=array(
                'lname.required'=>'Last Name is Required',
                'fname.required'=>'First Name is Required',
            );
            $rules=array(
                'image'=>'required|image',
                'lname'=>'required',
                'fname'=>'required',
                'bday'=>'required',
                'address'=>'required',
				'contact'=>'required',
                'email'=>'required|unique:coach',
                'password'=>'required'
            );
            $validator = Validator::make($request->all(), $rules, $messages)->validate();
            $coach = new Coach();

            if($request->hasFile('image')){
                $d = Carbon::now();
                $name = $request->get('lname').''.$d->day.str_random(4).$d->hour.$d->second;
                $p1 ='\images\\';
                $path = base_path() . '/public/images/';
                $request->file('image')->move($path,$name.'.'.$request->file('image')->getClientOriginalExtension());
                $coach->pic = $p1.$name.'.'.$request->file('image')->getClientOriginalExtension();
            }

            $coach->fill($request->all());
            $shit = Carbon::createFromFormat('Y-m-d', $coach->bday);
            $coach->age = $coach->age($shit);
            $userAthlete = new User();
            $userAthlete->faculty_id = $coach->email;
            $userAthlete->fullName = $coach->fname;
			//$userAthlete->contact = $coach->contact;
            $userAthlete->password = bcrypt($coach->password);
            $userAthlete->userType = "coach";
            $userAthlete->save();
            $coach->save();

            $athletes = Coach::get();

            foreach($athletes as $athlete)
            {
                if((User::where('faculty_id', $athlete->email))->count() >= 1)
                {
                    
                }
                else
                {
                    $userAthlete = new User();
                    $userAthlete->faculty_id = $athlete->email;
                    $userAthlete->fullName = $athlete->fname;
                    $userAthlete->password = bcrypt("admins");
                    $userAthlete->userType = "coach";
                    $userAthlete->save();
                }
            }

            return redirect()->route('coach.edit', $coach->id);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $test = User::where('id', Auth::id())->where('userType', 'admin')->count();
        $test2 = User::where('id', Auth::id())->where('userType', 'coach')->count();
        $events=Events::get();
        $info=Coach::where('id',$id)->get();

        if($info->first()->email == User::where('id', Auth::id())->get()->first()->faculty_id || (User::where('id', Auth::id())->get()->first()->userType == 'admin'))
        {
            if($test == 0 && $test2 == 0)
            {
                $shit = User::where('id', Auth::id())->get();
                foreach ($shit as $a) {
                    $pass = $a->faculty_id;
                }

                return redirect()->route('form.edit', Crypt::encrypt($pass));
            }
            else
            {
                if($info->count()) {
                    $info=$info->first();
                    $e = array();
                    if ($info->events->count()) {
                        foreach ($info->events as $pe) {
                            $e[$pe->name->first()->desc] = $pe->name->first()->desc;
                        }
                    }
                    $shit = Carbon::createFromFormat('Y-m-d', $info->bday);
                    $info->age = $info->age($shit);
                    $info->save();
                    return view('coaches.edit', compact('info', 'events', 'e'));
                } else
                    return redirect()->route('form.index')->with('messages','No Record Found');
            }
        }else if(Auth::check())
        {
            return redirect()->back();
        }
        else
            return redirect()->route('login');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $test = User::where('id', Auth::id())->where('userType', 'admin')->count();
        $test2 = User::where('id', Auth::id())->where('userType', 'coach')->count();

        if($test == 0 && $test2 == 0)
        {
            $shit = User::where('id', Auth::id())->get();
            foreach ($shit as $a) {
                $pass = $a->faculty_id;
            }

            return redirect()->route('form.edit', Crypt::encrypt($pass));
        }
        else
        {
            $messages=array(

                'lname.required'=>'Last Name is Required',
                'fname.required'=>'First Name is Required',
            );
            $rules=array(
                'image'=>'image',
                'lname'=>'required',
                'fname'=>'required',
                'bday'=>'required',
                'address'=>'required',
                'email'=>'required',
				'contact'=>'required',
                'password'=>'required'
                //'password'=>'required'
            );
            $validator=Validator::make($request->all(),$rules,$messages)->validate();
            $coach=Coach::where('id',$id)->get()->first();

            $oldEmail = $coach->email;
            
            if($request->hasFile('image')){
                $d=Carbon::now();
                $name=$request->get('lname').''.$d->day.str_random(4).$d->hour.$d->second;
                $p1='\images\\';
                $path= base_path() . '/public/images/';
                $request->file('image')->move($path,$name.'.'.$request->file('image')->getClientOriginalExtension());
                $coach->pic=$p1.$name.'.'.$request->file('image')->getClientOriginalExtension();
            }
            $coach->fill($request->all());

            $editedAdmin = User::where('faculty_id', $oldEmail)->get()->first();
			$editedAdmin->password = bcrypt($coach->password);
            $editedAdmin->faculty_id = $coach->email;
            $editedAdmin->save();

            $coach->save();
            return redirect()->route('coach.edit',$coach->id);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $test = User::where('id', Auth::id())->where('userType', 'admin')->count();
        $test2 = User::where('id', Auth::id())->where('userType', 'coach')->count();

        if($test == 0 && $test2 == 0)
        {
            $shit = User::where('id', Auth::id())->get();
            foreach ($shit as $a) {
                $pass = $a->faculty_id;
            }

            return redirect()->route('form.edit', Crypt::encrypt($pass));
        }
        else
        {
            $c=Coach::where('id',$id)->get()->first();
            $deleteAdmin = User::where('faculty_id', $c->email)->get()->first();
            $deleteAdmin->forceDelete();
            $c->delete();
            $c->events()->delete();
            return redirect()->route('coach.index');
        }
    }
}
