<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        '/',
        'adminList',
        'addAdmin',
        'api/user',
        'attachment',
        'attachment/*',
        'ce',
        'ce/*',
        'cl',
        'cl/*',
        'coach',
        'coach/*',
        'confirm/*',
        'deleteAdmin/*',
        'eve',
        'event',
        'event/*',
        'evet/*',
        'form',
        'form/*',
        'gallery',
        'gallery/*',
        'home',
        'login',
        'logout',
        'password/*',
        'print/*',
        'register',
        'search',
        'showResults/*',
        'unconfirmed',
        'editAdmin/*',
        'masterlist',
        'masterlist/*'
    ];
}
