<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

use Carbon\Carbon;

class PlayerInfo extends Model
{
    use SoftDeletes;
    //
    protected $table='playerinfo';

    protected $fillable=[
        'student_number',
        'lname',
        'fname',
        'mname',
        'weight',
        'age',
        'dob',
        'address',
        'allergies',
        'medication',
        'height',
        'contact',
        'blood_type',
        'pic',
        'scuaaCategory',
        'password'
    ];

    protected $dates=[
        'created_at',
        'deleted_at'
    ];

    public function elem(){
        return $this->hasMany('App\Educ','student_number','student_number')->where('level','Elem');
    }
    public function high(){
        return $this->hasMany('App\Educ','student_number','student_number')->where('level','High');
    }
    public function col(){
        return $this->hasMany('App\Educ','student_number','student_number')->where('level','Col');
    }
    public function trans(){
        return $this->hasMany('App\Educ','student_number','student_number')->where('level','Trans');
    }
    public function events(){
        return $this->hasMany('App\PlayerEvents','student_number','student_number');
    }
    public function educ(){
        return $this->hasMany('App\Educ','student_number','student_number');
    }
    public function documents(){
        return $this->hasMany('App\PlayerAttachments', 'student_number', 'student_number');
    }
    public function age($shit){
        return $shit->diffInYears(Carbon::now());
    }
}
