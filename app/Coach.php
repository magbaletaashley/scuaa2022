<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class Coach extends Model
{
    //
    protected $table='coach';
    protected $fillable=['lname','fname','mname','contact','email','bday','age','address', 'password'];
    public function events(){
        return $this->hasMany('App\CoachEvents','coach_id','id');
    }
    public function age($parameter){
    	return $parameter->diffInYears(Carbon::now());
    }
}
