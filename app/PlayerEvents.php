<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlayerEvents extends Model
{
    //
    protected $table='playerevents';

    protected $fillable=['student_number','events_id','status'];
    
    public function name(){
        return $this->hasMany('App\Events','id','events_id');
    }
    public function eve(){
        return $this->belongsTo('App\Events','id','events_id');
    }
    public function info(){
        return $this->belongsTo('App\PlayerInfo','student_number','student_number');
    }
}
