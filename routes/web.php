<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
   //return redirect()->route('login');
});

Auth::routes();

Route::post('/search',array('as'=>'ser','uses'=>'FormController@search'));
Route::post('/eve',array('as'=>'eve','uses'=>'FormController@eventSearch'));
Route::get('/evet/{id}',array('as'=>'evet','uses'=>'FormController@evet'));
Route::get('/evet2/{id}',array('as'=>'evet2','uses'=>'FormController@evet2'));
Route::get('/showResults/{id}', array('as' => 'showResults', 'uses' => 'FormController@showResults'));
Route::get('/addAdmin', array('as' => 'addAdmin', 'uses' => 'FormController@addAdmin'));
Route::post('/register', array('as' => 'register', 'uses' => 'FormController@register'));
Route::get('/adminList', array('as' => 'adminList', 'uses' => 'FormController@adminList'));
Route::delete('/deleteAdmin/{id}', array('as' => 'deleteAdmin', 'uses' => 'FormController@destroyAdmin'));
Route::get('/unconfirmed', array('as' => 'unconfirmed', 'uses' => 'FormController@unconfirmed'));
Route::post('/confirm/{id}', array('as' => 'confirm', 'uses' => 'FormController@confirm'));
Route::get('/home', 'FormController@index');
//Route::post('print', 'PrintController', array('only'=>array('show1')));

Route::resource('form','FormController');
Route::resource('event','EventController');
Route::resource('attachment', 'AttachmentsController');
Route::resource('print', 'PrintController', array('only'=>array('show')));
Route::resource('otr', 'OTRController', array('only'=>array('show')));

Route::resource('coach','CoachController');
Route::resource('ce','CEventsController');
Route::resource('gallery','GalleryController');
Route::resource('cl','CheckController');
Route::resource('masterlist', 'MasterListController');